#!/bin/sh -e
#
# Copyright (c) 2009-2014 Robert Nelson <robertcnelson@gmail.com>
#	modified by Fredy Alexander Suárez C <alxfx64@gmail.com>  
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

ARCH=$(uname -m)
DIR=$PWD
DOWNSUBDIR="dl"
CC_OUT="gcc-arm-linux-gnueabihf"

echo "======================================================================"
echo "DIR : ${DIR}"
echo "ARCH: ${ARCH}"
echo "======================================================================"

#toolchain="gcc_linaro_eabi_4_8"
#toolchain="gcc_linaro_eabi_4_9"

#toolchain="gcc_linaro_gnueabi_4_6"
toolchain="gcc_linaro_gnueabihf_4_7" #recomended for binary copability
#toolchain="gcc_linaro_gnueabihf_4_8"
#toolchain="gcc_linaro_gnueabihf_4_9"




download_gcc_generic () {
	#echo "==> download ${toolchain_name} : ${ARCH}"

	WGET="wget -c --directory-prefix=${DIR}/${DOWNSUBDIR}/"
	if [ ! -f ${DIR}/${DOWNSUBDIR}/${directory}/${datestamp} ] ; then
		echo "Installing: ${toolchain_name}"



		${WGET} ${site}/${version}/${filename}
		if [ -d ${DIR}/${DOWNSUBDIR}/${directory} ] ; then
			rm -rf ${DIR}/${DOWNSUBDIR}/${directory} || true
		fi
		tar -xf ${DIR}/${DOWNSUBDIR}/${filename} -C ${DIR}/${DOWNSUBDIR}/
		if [ -f ${DIR}/${DOWNSUBDIR}/${directory}/${binary}gcc ] ; then
			touch ${DIR}/${DOWNSUBDIR}/${directory}/${datestamp}
		fi
		
	fi

	if [ "x${ARCH}" = "xarmv7l" ] ; then
		#using native gcc
		CC=
	else
		CC="${DIR}/${CC_OUT}/${binary}" 

	fi
	echo "+LN ${DIR}/${DOWNSUBDIR}/${directory}"
	echo "TO ${DIR}/${CC_OUT}"

	if [ -d ${DIR}/${CC_OUT} ] ; then
			rm -rf ${DIR}/${CC_OUT} || true
	fi
	ln -s  ${DIR}/${DOWNSUBDIR}/${directory} ${DIR}/${CC_OUT}

}

#----------------------------------------------------------------------
gcc_toolchain () {
	site="https://releases.linaro.org"


	case "${toolchain}" in
	gcc_linaro_eabi_4_8)
		#https://releases.linaro.org/14.04/components/toolchain/binaries/gcc-linaro-arm-none-eabi-4.8-2014.04_linux.tar.xz
		gcc_version="4.8"
		release="2014.04"
		toolchain_name="gcc-linaro-arm-none-eabi"
		version="14.04/components/toolchain/binaries"
		directory="${toolchain_name}-${gcc_version}-${release}_linux"
		filename="${directory}.tar.xz"
		datestamp="${release}-${toolchain_name}"

		binary="bin/arm-none-eabi-"
		;;
	gcc_linaro_eabi_4_9)
		#https://releases.linaro.org/14.07/components/toolchain/binaries/gcc-linaro-arm-none-eabi-4.9-2014.07_linux.tar.xz
		gcc_version="4.9"
		release="2014.07"
		toolchain_name="gcc-linaro-arm-none-eabi"
		version="14.07/components/toolchain/binaries"
		directory="${toolchain_name}-${gcc_version}-${release}_linux"
		filename="${directory}.tar.xz"
		datestamp="${release}-${toolchain_name}"

		binary="bin/arm-none-eabi-"
		;;
	gcc_linaro_gnueabi_4_6)
		#https://releases.linaro.org/12.03/components/toolchain/binaries/gcc-linaro-arm-linux-gnueabi-2012.03-20120326_linux.tar.bz2
		release="2012.03"
		toolchain_name="gcc-linaro-arm-linux-gnueabi"
		version="12.03/components/toolchain/binaries"
		version_date="20120326"
		directory="${toolchain_name}-${release}-${version_date}_linux"
		filename="${directory}.tar.bz2"
		datestamp="${version_date}-${toolchain_name}"

		binary="bin/arm-linux-gnueabi-"
		;;
	gcc_linaro_gnueabihf_4_7)
		#https://releases.linaro.org/13.04/components/toolchain/binaries/gcc-linaro-arm-linux-gnueabihf-4.7-2013.04-20130415_linux.tar.xz
		gcc_version="4.7"
		release="2013.04"
		toolchain_name="gcc-linaro-arm-linux-gnueabihf"
		version="13.04/components/toolchain/binaries"
		version_date="20130415"
		directory="${toolchain_name}-${gcc_version}-${release}-${version_date}_linux"
		filename="${directory}.tar.xz"
		datestamp="${version_date}-${toolchain_name}"

		binary="bin/arm-linux-gnueabihf-"
		;;
	gcc_linaro_gnueabihf_4_8)
		#https://releases.linaro.org/14.04/components/toolchain/binaries/gcc-linaro-arm-linux-gnueabihf-4.8-2014.04_linux.tar.xz
		gcc_version="4.8"
		release="2014.04"
		toolchain_name="gcc-linaro-arm-linux-gnueabihf"
		version="14.04/components/toolchain/binaries"
		directory="${toolchain_name}-${gcc_version}-${release}_linux"
		filename="${directory}.tar.xz"
		datestamp="${release}-${toolchain_name}"

		binary="bin/arm-linux-gnueabihf-"
		;;
	gcc_linaro_gnueabihf_4_9)
		#https://releases.linaro.org/14.07/components/toolchain/binaries/gcc-linaro-arm-linux-gnueabihf-4.9-2014.07_linux.tar.xz
		gcc_version="4.9"
		release="2014.07"
		toolchain_name="gcc-linaro-arm-linux-gnueabihf"
		version="14.07/components/toolchain/binaries"
		directory="${toolchain_name}-${gcc_version}-${release}_linux"
		filename="${directory}.tar.xz"
		datestamp="${release}-${toolchain_name}"

		binary="bin/arm-linux-gnueabihf-"
		;;
	*)
		echo "bug: maintainer forgot to set:"
		echo "toolchain=\"xzy\" in version.sh"
		exit 1
		;;
	esac

echo "DOWNLOAD: ${filename}" 
echo "======================================================================"
	download_gcc_generic
}

if [ -d ${DIR}/${CC_OUT}} ] ; then
	#mv ${DIR}/${CC_OUT} ${DIR}/${CC_OUT}_old
	rm -rf ${DIR}/${CC_OUT} || true
fi

if [ "x${CC}" = "x" ] && [ "x${ARCH}" != "xarmv7l" ] ; then
	gcc_toolchain
fi

echo "-GCC READY TO USE-"

