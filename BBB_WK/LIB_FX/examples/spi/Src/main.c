/* 
 * File:   main.c
 * Author: alx
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"

uint16_t spio=0x1;
/*
 * 
 */
int main() {
    Load_libfx(); 
    LoadSPI(SPI1);
    setSpeedSPI(SPI1,1000000);
    setBitsPerWordSPI(SPI1,16);
    printf("spio %d\r\n",spio);
    while (1) {
        spio++;
        WriteReadSPI(SPI1, &spio, 1);
        printf("spio %d\r\n", spio);
        if(spio>=32767){
            spio=0;
        }
    }
    return (EXIT_SUCCESS);
}