
#ifndef STM_SSP_H
#define STM_SSP_H

#ifndef STDARG_H
#define STDARG_H

#ifndef _VA_LIST_DEFINED
#define _VA_LIST_DEFINED
typedef char *va_list;
#endif

#define _FLSIZEOF(n)    ((sizeof(n) + sizeof(float) - 1) & ~(sizeof(float) - 1))
#define VA_START(ap, v)  (ap = (va_list) &v + _FLSIZEOF(v))
#define VA_ARG(ap, t)    (*(t *)((ap += _FLSIZEOF(t)) - _FLSIZEOF(t)))
#define VA_END(ap)       (ap = (va_list) 0)

#endif


int a2d(char ch);
char *i2a(unsigned i, char *a, unsigned r);
char *if2a(unsigned i, char *a, unsigned digits);
char *itoa(int i, char *a, int r);
int ftoax( float f, unsigned int digits, char *buf);
int check_isdigit (char c);
int check_isupper(char c);
int check_isspace(int c);
int sscanf_atoi(const char *str, int str_sz, int radix);
float sscanf_atof(const char *str, int str_sz);
int sscanf_scanf(const char *buff, int buff_sz, const char *format, ...);


void _print_c(int c);
int _print_s(const char *string, int width, int pad);
int _print_i(int i, int b, int sg, int width, int pad);
int _print_f(float f, int width, int precision, int pad);
int _VPrintf(const char *format, va_list args );
int _VSnprintf(char *out, int size, const char *format, va_list args);
int Printf(const char *format, ...);
int Snprintf(char *out, int size, const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif
