/* 
 * File:   main.c
 * Author: alx
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */
 /*
GND     P9_1
VCC     P9_3
PWMA    P9_22
PWMB    P9_21
 */

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"
#include "BBB_pwm.h"

int DUTY = 1;
int flag_updown = 0;


/*
 * 
 */
int main() {
    Load_libfx();
    LoadPWM(P9_21);
    LoadPWM(P9_22);
    setStopPWM(P9_22);
    setStopPWM(P9_22);
    setFrecuencyPWM(P9_21, 400);
    setFrecuencyPWM(P9_22, 400);
    setPolarityPWM(P9_21, 1);
    setPolarityPWM(P9_22, 1);
    setDutyPWM(P9_21, 0);
    setDutyPWM(P9_22, 0);
    setStartPWM(P9_21);
    setStartPWM(P9_22);
    while (1) {
        if (DUTY >= 100) {
            printf("now down\r\n");
            DUTY=100;
            flag_updown = 1;
            
        }
        if (DUTY <= 0) {
            printf("now up\r\n");
            DUTY=0;
            flag_updown = 0;
        }
       //usleep(10000);
        if ((DUTY < 100 )& (flag_updown == 0)) {
            setDutyPWM(P9_21,100- DUTY);
            setDutyPWM(P9_22,100- DUTY);
            printf("u DYTY %d\r\n",DUTY);
            DUTY+=1;
        } else if ((DUTY > 0) & (flag_updown == 1)) {
            setDutyPWM(P9_21,100- DUTY);
            setDutyPWM(P9_22,100- DUTY);
            printf("d DYTY %d\r\n", DUTY);
            DUTY-=1;
        }
        usleep(500000);

    }
    return (EXIT_SUCCESS);
}

