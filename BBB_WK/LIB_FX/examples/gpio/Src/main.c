/* 
 * File:   main.c
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"

/*
 * 
 */
int main() {
    Load_libfx();
    printf("GPIO1  mux mode configuration: %X\n", getGpioMuxMode(P8_11));
    printf("GPIO1  pull en configuration: %X\n", getGpioPullEn(P8_11));
    printf("GPIO1  pull type configuration: %X\n", getGpioPullType(P8_11));
    printf("GPIO1  rxactive configuration: %X\n", getGpioRxEn(P8_11));
    printf("GPIO1  slewctrl configuration: %X\n", getGpioSlewRate(P8_11));
    LoadGpioPin(P8_11, OUTPUT);
    while (1) {
        printf("ON\r\n");
        writeGpioPin(P8_11, HIGH);
        usleep(500000);
        printf("OFF\r\n");
        writeGpioPin(P8_11, LOW);
        usleep(500000);
    }
    return (EXIT_SUCCESS);
}

