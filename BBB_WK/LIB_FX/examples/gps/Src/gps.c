/*
 *
 * NMEA library
 * URL: http://nmea.sourceforge.net
 * Author: Tim (xtimor@gmail.com)
 * Licence: http://www.gnu.org/licenses/lgpl.html
 * $Id: parse.c 17 2008-03-11 11:56:11Z xtimor $
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "stm_ssp.h"
#include "gps.h"

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param buff [description]
 * @param buff_sz [description]
 * @param res [description]
 * @return [description]
 */
int _nmea_parse_time(const char *buff, int buff_sz, nmeaTIME *res) {
    int success = 0;

    switch (buff_sz) {
        case sizeof ("hhmmss") - 1:
            success = (3 == sscanf_scanf(buff, buff_sz, "%2d%2d%2d", &(res->hour), &(res->min), &(res->sec)
                    ));
            break;
        case sizeof ("hhmmss.s") - 1:
        case sizeof ("hhmmss.ss") - 1:
        case sizeof ("hhmmss.sss") - 1:
            success = (4 == sscanf_scanf(buff, buff_sz, "%2d%2d%2d.%d", &(res->hour), &(res->min), &(res->sec), &(res->hsec)
                    ));
            break;
        default:
            printf("Parse of time error (format error)!\r\n");
            success = 0;
            break;
    }

    return (success ? 0 : -1);
}
/***********************************************************************/

/**
 * \brief Define packet type by header (nmeaPACKTYPE).
 * @param buff a constant character pointer of packet buffer.
 * @param buff_sz buffer size.
 * @return The defined packet type
 * @see nmeaPACKTYPE
 */
int nmea_pack_type(const char *buff, int buff_sz) {
    static const char *pheads[] ={
        "$GPGGA",
        "$GPGSA",
        "$GPGSV",
        "$GPRMC",
        "$GPVTG",
    };

    assert(buff);

    if (buff_sz < 5)
        return 0;
    else if (0 == memcmp(buff, pheads[0], 6))
        return 1;
    else if (0 == memcmp(buff, pheads[1], 6))
        return 2;
    else if (0 == memcmp(buff, pheads[2], 6))
        return 3;
    else if (0 == memcmp(buff, pheads[3], 6))
        return 4;
    else if (0 == memcmp(buff, pheads[4], 6))
        return 5;

    return 6;
}

/**
 * \brief Parse GGA packet from buffer.
 * @param buff a constant character pointer of packet buffer.
 * @param buff_sz buffer size.
 * @param pack a pointer of packet which will filled by function.
 * @return 1 (true) - if parsed successfully or 0 (false) - if fail.
 */
int nmea_parse_GPGGA(const char *buff, int buff_sz, nmeaGPGGA *pack) {
    char time_buff[NMEA_TIMEPARSE_BUF];

    assert(buff && pack);

    memset(pack, 0, sizeof (nmeaGPGGA));

    //nmea_trace_buff(buff, buff_sz);

    if (14 != sscanf_scanf(buff, buff_sz, "$GPGGA,%s,%f,%C,%f,%C,%d,%d,%f,%f,%C,%f,%C,%f,%d*",
            &(time_buff[0]),
            &(pack->lat), &(pack->ns), &(pack->lon), &(pack->ew),
            &(pack->sig), &(pack->satinuse), &(pack->HDOP), &(pack->elv), &(pack->elv_units),
            &(pack->diff), &(pack->diff_units), &(pack->dgps_age), &(pack->dgps_sid))) {
        printf("GPGGA parse error!\r\n");
        return 0;
    }

    if (0 != _nmea_parse_time(&time_buff[0], (int) strlen(&time_buff[0]), &(pack->utc))) {
        printf("GPGGA time parse error!\r\n");
        return 0;
    }

    return 1;
}

/**
 * \brief Parse GSA packet from buffer.
 * @param buff a constant character pointer of packet buffer.
 * @param buff_sz buffer size.
 * @param pack a pointer of packet which will filled by function.
 * @return 1 (true) - if parsed successfully or 0 (false) - if fail.
 */
int nmea_parse_GPGSA(const char *buff, int buff_sz, nmeaGPGSA *pack) {
    assert(buff && pack);

    memset(pack, 0, sizeof (nmeaGPGSA));

    if (17 != sscanf_scanf(buff, buff_sz, "$GPGSA,%C,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f*",
            &(pack->fix_mode), &(pack->fix_type),
            &(pack->sat_prn[0]), &(pack->sat_prn[1]), &(pack->sat_prn[2]), &(pack->sat_prn[3]), &(pack->sat_prn[4]), &(pack->sat_prn[5]),
            &(pack->sat_prn[6]), &(pack->sat_prn[7]), &(pack->sat_prn[8]), &(pack->sat_prn[9]), &(pack->sat_prn[10]), &(pack->sat_prn[11]),
            &(pack->PDOP), &(pack->HDOP), &(pack->VDOP))) {
        printf("GPGSA parse error!\r\n");
        return 0;
    }

    return 1;
}

/**
 * \brief Parse GSV packet from buffer.
 * @param buff a constant character pointer of packet buffer.
 * @param buff_sz buffer size.
 * @param pack a pointer of packet which will filled by function.
 * @return 1 (true) - if parsed successfully or 0 (false) - if fail.
 */
int nmea_parse_GPGSV(const char *buff, int buff_sz, nmeaGPGSV *pack) {
    int nsen, nsat;

    assert(buff && pack);

    memset(pack, 0, sizeof (nmeaGPGSV));

    nsen = sscanf_scanf(buff, buff_sz,
            "$GPGSV,%d,%d,%d,"
            "%d,%d,%d,%d,"
            "%d,%d,%d,%d,"
            "%d,%d,%d,%d,"
            "%d,%d,%d,%d*",
            &(pack->pack_count), &(pack->pack_index), &(pack->sat_count),
            &(pack->sat_data[0].id), &(pack->sat_data[0].elv), &(pack->sat_data[0].azimuth), &(pack->sat_data[0].sig),
            &(pack->sat_data[1].id), &(pack->sat_data[1].elv), &(pack->sat_data[1].azimuth), &(pack->sat_data[1].sig),
            &(pack->sat_data[2].id), &(pack->sat_data[2].elv), &(pack->sat_data[2].azimuth), &(pack->sat_data[2].sig),
            &(pack->sat_data[3].id), &(pack->sat_data[3].elv), &(pack->sat_data[3].azimuth), &(pack->sat_data[3].sig));

    nsat = (pack->pack_index - 1) * NMEA_SATINPACK;
    nsat = (nsat + NMEA_SATINPACK > pack->sat_count) ? pack->sat_count - nsat : NMEA_SATINPACK;
    nsat = nsat * 4 + 3 /* first three sentence`s */;

    if (nsen < nsat || nsen > (NMEA_SATINPACK * 4 + 3)) {
        printf("GPGSV parse error!\r\n");
        return 0;
    }

    return 1;
}

/**
 * \brief Parse RMC packet from buffer.
 * @param buff a constant character pointer of packet buffer.
 * @param buff_sz buffer size.
 * @param pack a pointer of packet which will filled by function.
 * @return 1 (true) - if parsed successfully or 0 (false) - if fail.
 */
int nmea_parse_GPRMC(const char *buff, int buff_sz, nmeaGPRMC *pack) {
    int nsen;
    char time_buff[NMEA_TIMEPARSE_BUF];

    assert(buff && pack);

    memset(pack, 0, sizeof (nmeaGPRMC));


    nsen = sscanf_scanf(buff, buff_sz, "$GPRMC,%s,%C,%f,%C,%f,%C,%f,%f,%2d%2d%2d,%f,%C,%C*",
            &(time_buff[0]),
            &(pack->status), &(pack->lat), &(pack->ns), &(pack->lon), &(pack->ew),
            &(pack->speed), &(pack->direction),
            &(pack->utc.day), &(pack->utc.mon), &(pack->utc.year),
            &(pack->declination), &(pack->declin_ew), &(pack->mode));

    if (nsen != 13 && nsen != 14) {
        printf("GPRMC parse error!\r\n");
        return 0;
    }

    if (0 != _nmea_parse_time(&time_buff[0], (int) strlen(&time_buff[0]), &(pack->utc))) {
        printf("GPRMC time parse error!\r\n");
        return 0;
    }
    //printf("day %d ,mon %d , year %d\r\n",pack->utc.day,pack->utc.mon,pack->utc.year);
    //if (pack->utc.year < 90)
    //pack->utc.year += 100;
    //pack->utc.mon -= 1;
    //printf("day %d ,mon %d , year %d\r\n",pack->utc.day,pack->utc.mon,pack->utc.year);
    return 1;
}

/**
 * \brief Parse VTG packet from buffer.
 * @param buff a constant character pointer of packet buffer.
 * @param buff_sz buffer size.
 * @param pack a pointer of packet which will filled by function.
 * @return 1 (true) - if parsed successfully or 0 (false) - if fail.
 */
int nmea_parse_GPVTG(const char *buff, int buff_sz, nmeaGPVTG *pack) {
    assert(buff && pack);

    memset(pack, 0, sizeof (nmeaGPVTG));

    //nmea_trace_buff(buff, buff_sz);

    if (8 != sscanf_scanf(buff, buff_sz, "$GPVTG,%f,%C,%f,%C,%f,%C,%f,%C*",
            &(pack->dir), &(pack->dir_t),
            &(pack->dec), &(pack->dec_m),
            &(pack->spn), &(pack->spn_n),
            &(pack->spk), &(pack->spk_k))) {
        printf("GPVTG parse error!\r\n");
        return 0;
    }

    if (pack->dir_t != 'T' ||
            pack->dec_m != 'M' ||
            pack->spn_n != 'N' ||
            pack->spk_k != 'K') {
        printf("GPVTG parse error (format error)!\r\n");
        return 0;
    }

    return 1;
}