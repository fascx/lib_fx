/* 
 * File:   main.c
 * Author: Fredy Alexander Suárez Cárdenas 
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"


/*
 * 
 */
int main() {
    Load_libfx();
    LoadADC();
    float ain0 = 0;
    while (1) {
        usleep(100000);
        readADC(AN0, &ain0);
        printf("ain0:%f\r\n", ain0);
    }
    return (EXIT_SUCCESS);
}

