/* 
 * File:   main.c
 * Author: alx
 *
 * Created on 7 de diciembre de 2013, 10:28 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "BBB.h"

uint8_t i2cData[14];
int16_t accX = 0, accY = 0, accZ = 0;
int16_t tempRaw = 0;
int16_t gyroX = 0, gyroY = 0, gyroZ = 0;

/*
 * 
 */
int main() {
    Load_libfx();
    LoadI2C(I2C1);
    i2cData[0] = 7; // Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
    i2cData[1] = 0x00; // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
    i2cData[2] = 0x00; // Set Gyro Full Scale Range to ±250deg/s
    i2cData[3] = 0x00; // Set Accelerometer Full Scale Range to ±2g
    while (i2cWriteBytes(I2C1, 0x68, 0x19, &i2cData[0], 4) == 0); // Write to all four registers at once
    while (i2cWriteByte(I2C1, 0x68, 0x6B, 0x01) == 0); // PLL with X axis gyroscope reference and disable sleep mode 
    while (i2cReadByte(I2C1, 0x68, 0x75, &i2cData[0]) == 0);
    if (i2cData[0] != 0x68) { // Read "WHO_AM_I" register
        printf("Error reading sensor\r\n");
        return 0;
    } else {
        printf("ok i2c\r\n");
    }
    while (1) {
        usleep(50000);
        while (i2cReadBytes(I2C1, 0x68, 0x3B, i2cData, 14) == 0);
        accX = ((i2cData[0] << 8) | i2cData[1]);
        accY = ((i2cData[2] << 8) | i2cData[3]);
        accZ = ((i2cData[4] << 8) | i2cData[5]);
        tempRaw = ((i2cData[6] << 8) | i2cData[7]);
        gyroX = ((i2cData[8] << 8) | i2cData[9]);
        gyroY = ((i2cData[10] << 8) | i2cData[11]);
        gyroZ = ((i2cData[12] << 8) | i2cData[13]);
        printf("a/g: %6hd %6hd %6hd   %6hd %6hd %6hd %10hd\n", accX, accY, accZ, gyroX, gyroY, gyroZ, tempRaw);
    }
    return (EXIT_SUCCESS);
}

