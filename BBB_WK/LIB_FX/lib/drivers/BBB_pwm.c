/**
 ******************************************************************************
 * @file
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"

#include "BBB_HEADER.h"

#define KEYLEN 32

#define PERIOD 0
#define DUTY 1

int8_t OCP_DIR[128];
int16_t pwm_initialized = 0;

/******************************************************************************/
struct pwm_exp
{
    int16_t MODULE;
    int16_t GPIO;

    int16_t period_fd;
    int16_t duty_fd;
    int16_t polarity_fd;
    int16_t run_fd;

    uint32_t duty;
    uint32_t period_ns;
    struct pwm_exp *next;
};
struct pwm_exp *exported_pwms = NULL;
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @return [description]
 */
struct pwm_exp *lookup_exported_pwm(int16_t GpioPin)
{
    struct pwm_exp *pwm = exported_pwms;

    while (pwm != NULL)
    {
        if (pwm->GPIO == GpioPin)
        {
            return pwm;
        }
        pwm = pwm->next;
    }

    return NULL; /* standard for pointers */
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 * @return [description]
 */
ErrorStatus load_dev_pwm(void)
{
    if (!pwm_initialized && load_device_tree((char *) PWM_CAPE))
    {
        build_path((char *) "/sys/devices", "ocp", (char *) OCP_DIR, sizeof (OCP_DIR));
        pwm_initialized = 1;
        return SUCCESS;
    }
    return ERROR;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @return [description]
 */
ErrorStatus LoadDevTree(int16_t GpioPin)
{
    int8_t DEV_TREE_KEY[18];
    int8_t *key = (int8_t *) BBB_HEADER[GpioPin].KEY;
    snprintf((char *) DEV_TREE_KEY, sizeof (DEV_TREE_KEY), "LIBFX_PWM_%s", key);

    if (!load_device_tree((char *) DEV_TREE_KEY))
    {
        printf("ERROR LOADING DT\r\n");
        return ERROR;
    }
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @return [description]
 */
ErrorStatus LoadPWM(int16_t GpioPin)
{

    int8_t PWM_PIN_KEY[128];
    int8_t PWM_PATH_KEY[128];
    int8_t PERIOD_PATH[128];
    int8_t DUTY_PATH[128];
    int8_t POLARITY_PATH[128];
    int8_t RUN_PATH[128];
    int16_t period_fd, duty_fd, polarity_fd, run_fd;
    struct pwm_exp *new_pwm, *pwm;

    /*1)load primary capes ans key*/
    int8_t *key = (int8_t *) BBB_HEADER[GpioPin].KEY;
    if (!pwm_initialized)
    {
        load_dev_pwm();
    }
    /*2)load cape*/
    /*-------------------------------------------------------------------------*/
    LoadDevTree(GpioPin);
    /*-------------------------------------------------------------------------*/
    /*3)create path*/
    /*-------------------------------------------------------------------------*/
    snprintf((char *) PWM_PIN_KEY, sizeof (PWM_PIN_KEY), "pwm_test_%s", key);
    build_path((char *) OCP_DIR, (char *) PWM_PIN_KEY, (char *) PWM_PATH_KEY, sizeof (PWM_PATH_KEY));
    printf("path:%s\n", PWM_PATH_KEY);
    snprintf((char *) PERIOD_PATH, sizeof (PERIOD_PATH), "%s/period", PWM_PATH_KEY);
    snprintf((char *) DUTY_PATH, sizeof (DUTY_PATH), "%s/duty", PWM_PATH_KEY);
    snprintf((char *) POLARITY_PATH, sizeof (POLARITY_PATH), "%s/polarity", PWM_PATH_KEY);
    snprintf((char *) RUN_PATH, sizeof (RUN_PATH), "%s/run", PWM_PATH_KEY);
    printf("period:%s\n", PERIOD_PATH);
    printf("duty:%s\n", DUTY_PATH);
    printf("polarity:%s\n", POLARITY_PATH);
    printf("run:%s\n", RUN_PATH);
    /*-------------------------------------------------------------------------*/
    /*4)open path*/
    /*-------------------------------------------------------------------------*/
    if ((period_fd = open((char *) PERIOD_PATH, O_RDWR)) < 0)
    {
        printf("E period_fd\r\n");
        return ERROR;
    }

    if ((duty_fd = open((char *) DUTY_PATH, O_RDWR)) < 0)
    {
        printf("E duty_fd\r\n");
        close(period_fd);
        return ERROR;
    }

    if ((polarity_fd = open((char *) POLARITY_PATH, O_RDWR)) < 0)
    {
        printf("E polarity_fd\r\n");
        close(period_fd);
        close(duty_fd);
        return ERROR;
    }

    if ((run_fd = open((char *) RUN_PATH, O_RDWR)) < 0)
    {
        printf("E polarity_fd\r\n");
        close(period_fd);
        close(duty_fd);
        close(polarity_fd);
        return ERROR;
    }
    /*-------------------------------------------------------------------------*/
    /*insert elemnets in list*/
    /*-------------------------------------------------------------------------*/
    new_pwm = malloc(sizeof (struct pwm_exp));
    if (new_pwm == 0)
    {
        printf("E malloc\r\n");
        return ERROR; // out of memory
    }

    new_pwm->GPIO = GpioPin;
    new_pwm->period_fd = period_fd;
    new_pwm->duty_fd = duty_fd;
    new_pwm->polarity_fd = polarity_fd;
    new_pwm->polarity_fd = run_fd;
    new_pwm->next = NULL;

    if (exported_pwms == NULL)
    {
        // create new list
        exported_pwms = new_pwm;
    }
    else
    {
        // add to end of existing list
        pwm = exported_pwms;
        while (pwm->next != NULL)
            pwm = pwm->next;
        pwm->next = new_pwm;
    }
    printf("KEY ok\r\n");
    /*-------------------------------------------------------------------------*/
    return SUCCESS;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @return [description]
 */
ErrorStatus pwm_disable(int16_t GpioPin)
{
    struct pwm_exp *pwm = NULL, *temp = NULL, *prev_pwm = NULL;
    pwm = exported_pwms;
    while (pwm != NULL)
    {
        if (pwm->GPIO == GpioPin)
        {

            close(pwm->period_fd);
            close(pwm->duty_fd);
            close(pwm->polarity_fd);

            if (prev_pwm == NULL)
            {

                exported_pwms = pwm->next;
                prev_pwm = pwm;
            }
            else
            {
                prev_pwm->next = pwm->next;
            }

            temp = pwm;
            pwm = pwm->next;
            free(temp);
        }
        else
        {
            prev_pwm = pwm;
            pwm = pwm->next;
        }
    }
    return SUCCESS;
}
/******************************************************************************/

/**
 *
 */
void closePWM(void)
{
    while (exported_pwms != NULL)
    {
        pwm_disable(exported_pwms->GPIO);
    }
}
/********************************API*******************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @param freq [description]
 *
 * @return [description]
 */
ErrorStatus setFrecuencyPWM(int16_t GpioPin, const float freq)
{
    int16_t len;
    int8_t buffer[20];
    uint32_t period_ns;
    struct pwm_exp *pwm;
    if (freq <= 0.0)
    {
        printf("freq <= 0.0\r\n");
        return ERROR;
    }
    pwm = lookup_exported_pwm(GpioPin);

    if (pwm == NULL)
    {
        printf("pwm == NULL\r\n");
        return ERROR;
    }
    period_ns = (uint32_t) (1e9 / freq);
    if (period_ns != pwm->period_ns)
    {
        pwm->period_ns = period_ns;
        len = snprintf((char *) buffer, sizeof (buffer), "%lu", (long unsigned int) period_ns);
        write(pwm->period_fd, buffer, len);
    }

    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @param polarity [description]
 *
 * @return [description]
 */
ErrorStatus setPolarityPWM(int16_t GpioPin, int16_t polarity)
{
    int16_t len;
    int8_t buffer[7];
    struct pwm_exp *pwm;
    pwm = lookup_exported_pwm(GpioPin);
    if (pwm == NULL)
    {
        return ERROR;
    }
    len = snprintf((char *) buffer, sizeof (buffer), "%d", polarity);
    if (write(pwm->polarity_fd, buffer, len) <= 0)
        return ERROR;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @param duty [description]
 *
 * @return [description]
 */
ErrorStatus setDutyPWM(int16_t GpioPin, const float duty)
{
    int16_t len;
    int8_t buffer[20];
    struct pwm_exp *pwm;
    if (duty < 0.0 || duty > 100.0)
    {
        printf("duty EEE :%g\r\n", duty);
        return ERROR;
    }
    pwm = lookup_exported_pwm(GpioPin);
    if (pwm == NULL)
    {
        printf("pwm == NULL\r\n");
        return ERROR;
    }
    pwm->duty = (uint32_t) (pwm->period_ns * (duty / 100.0));
    len = snprintf((char *) buffer, sizeof (buffer), "%lu", (long unsigned int) pwm->duty);
    if (write(pwm->duty_fd, buffer, len) <= 0)
        return ERROR;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @param run [description]
 *
 * @return [description]
 */
ErrorStatus runPWM(int16_t GpioPin, int16_t run)
{
    int16_t len;
    int8_t buffer[7];
    struct pwm_exp *pwm;
    pwm = lookup_exported_pwm(GpioPin);
    if (pwm == NULL)
    {
        return ERROR;
    }
    len = snprintf((char *) buffer, sizeof (buffer), "%d", run);
    if (write(pwm->run_fd, buffer, len) <= 0)
        return ERROR;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @return [description]
 */
ErrorStatus setStartPWM(int16_t GpioPin)
{
    return runPWM(GpioPin, 1);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param GpioPin [description]
 * @return [description]
 */
ErrorStatus setStopPWM(int16_t GpioPin)
{
    return runPWM(GpioPin, 0);
}
