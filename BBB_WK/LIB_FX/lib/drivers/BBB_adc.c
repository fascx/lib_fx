/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"


char adc_prefix_dir[40];
char OCP_DIR[25];
int ADC_INIT = 0;
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param d [description]
 * @return [description]
 */
ErrorStatus load_dev_adc(void) {
    if (ADC_INIT) {
        return SUCCESS;
    }
    if (load_device_tree("ADC_CAPE")) {
        build_path("/sys/devices", "ocp.", OCP_DIR, sizeof (OCP_DIR));
        build_path(OCP_DIR, "helper.", adc_prefix_dir, sizeof (adc_prefix_dir));
        strncat(adc_prefix_dir, "/AIN", sizeof (adc_prefix_dir));

        // Test that the directory has an AIN entry (found correct devicetree)
        char test_path[40];
        snprintf(test_path, sizeof (test_path), "%s%d", adc_prefix_dir, 0);

        FILE *fh = fopen(test_path, "r");

        if (!fh) {
            return ERROR;
        }
        fclose(fh);

        ADC_INIT = 1;
        return SUCCESS;
    }

    return ERROR;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param d [description]
 * @return [description]
 */
ErrorStatus LoadADC(void) {
    return load_dev_adc();
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 */
void adc_disable(void) {
    unload_device_tree("ADC_CAPE");
}

/******************************************************************************/
void closeADC(void) {

}


/********************************API*******************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param int [description]
 * @param value [description]
 *
 * @return [description]
 */
ErrorStatus readADC(unsigned int ain, float *value) {
    FILE *fh;
    char ain_path[40];
    int err, try_count = 0;
    int read_successful;
    snprintf(ain_path, sizeof (ain_path), "%s%d", adc_prefix_dir, ain);

    read_successful = 0;

    // Workaround to AIN bug where reading from more than one AIN would cause access failures
    while (!read_successful && try_count < 3) {
        fh = fopen(ain_path, "r");

        // Likely a bad path to the ocp device driver
        if (!fh) {
            return ERROR;
        }

        fseek(fh, 0, SEEK_SET);
        err = fscanf(fh, "%f", value);

        if (err != EOF) read_successful = 1;
        fclose(fh);

        try_count++;
    }

    if (read_successful) {
        return SUCCESS;
    }

    // Fall through and fail
    return ERROR;
}
