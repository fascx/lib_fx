/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"



#define KEYLEN 32

int8_t ocp_dir[32];
int8_t spi_dir[128];
int16_t spi_initialized[2] = {0, 0};

/******************************************************************************/
struct spi_exp {
    int16_t SPI;
    int16_t spi_fd;
    uint8_t mode;
    uint8_t bitsPerWord;
    uint32_t speed;
    struct spi_exp *next;
};
struct spi_exp *exported_spi = NULL;
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @return [description]
 */
struct spi_exp *lookup_exported_spi(uint8_t SPIMOD) {
    struct spi_exp *spi = exported_spi;

    while (spi != NULL) {
        if (spi->SPI == (SPIMOD + 1)) {
            return spi;
        }
        spi = spi->next;
    }
    return NULL; /* standard for pointers */
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @return [description]
 */
ErrorStatus load_dev_spi(uint8_t SPIMOD) {
    int16_t status = 0;
    if (SPIMOD == 0) {
        printf("spi0\r\n");
        //
        if (!spi_initialized[0] && load_device_tree((char *) SPI0_CAPE)) {
            printf("spi0:ok\r\n");
            spi_initialized[0] = 1;
            status = 1;
        }
    } else if (SPIMOD == 1 && load_device_tree((char *) SPI1_CAPE)) {
        if (!spi_initialized[1]) {

            spi_initialized[1] = 1;
            status = 1;
        }
    } else {
        printf("ERROR INIT SPI \r\n");
        return ERROR;
    }
    if (status == 1) {
        build_path((char *) "/sys/devices", "ocp", (char *) ocp_dir, sizeof (ocp_dir));
        return SUCCESS;
    }
    return ERROR;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @return [description]
 */
ErrorStatus LoadSPI(int16_t SPIMOD) {
    int8_t *spi_path_p = NULL;
    int16_t spi_fd;
    //int8_t *key = NULL;
    struct spi_exp *new_spi = NULL, *spi = NULL;
    /*1)load primary capes ans key*/
    /*2)load cape*/
    /*-------------------------------------------------------------------------*/
    if (!spi_initialized[SPIMOD]) {
        load_dev_spi(SPIMOD);
    }
    /*-------------------------------------------------------------------------*/
    /*3)create path*/
    /*-------------------------------------------------------------------------*/
    if (SPIMOD == 0) {
        spi_path_p = (int8_t *) SPI0_PATH;
    } else if (SPIMOD == 1) {
        spi_path_p = (int8_t *) SPI1_PATH;
    } else {
        printf("ERROR SPI #\r\n");
        return ERROR;
    }

    //snprintf((char *) spi_path, sizeof (spi_path), "%s", spi_path_p);
    printf("enable_path:%s\r\n", spi_path_p);
    /*-------------------------------------------------------------------------*/
    /*4)open path*/
    /*-------------------------------------------------------------------------*/
    if ((spi_fd = open((char *) spi_path_p, O_RDWR)) < 0) {
        //error, close already opened period_fd.
        printf("E spi_fd\r\n");
        return ERROR;
    }
    printf("fd:%d\r\n", spi_fd);
    /*-------------------------------------------------------------------------*/
    /*insert elemnets in list*/
    /*-------------------------------------------------------------------------*/
    printf("KEY\r\n");
    new_spi = malloc(sizeof (struct spi_exp));
    if (new_spi == 0) {
        printf("E malloc\r\n");
        return ERROR; // out of memory
    }

    new_spi->SPI = SPIMOD + 1;
    new_spi->spi_fd = spi_fd;
    new_spi->mode = SPI_MODE_0;
    new_spi->bitsPerWord = 8;
    new_spi->speed = 1000000;
    new_spi->next = NULL;
    printf("fd:%d\r\n", new_spi->spi_fd);
    printf("KEY3\r\n");
    if (exported_spi == NULL) {
        //         create new list
        exported_spi = new_spi;
        printf("KEY4\r\n");
        return SUCCESS;
    } else {
        // add to end of existing list
        spi = exported_spi;
        while (spi->next != NULL)
            spi = spi->next;
        spi->next = new_spi;
        printf("KEY5\r\n");
    }
    printf("KEY6\r\n");
    /*-------------------------------------------------------------------------*/
    setDefaulCfgSPI(SPIMOD);
    return SUCCESS;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @return [description]
 */
ErrorStatus spi_disable(int16_t SPIMOD) {
    struct spi_exp *spi, *temp, *prev_spi = NULL;

    spi = exported_spi;
    while (spi != NULL) {
        if (spi->SPI == (SPIMOD + 1)) {

            close(spi->spi_fd);
            if (prev_spi == NULL) {
                exported_spi = spi->next;
                prev_spi = spi;
            } else {
                prev_spi->next = spi->next;
            }
            temp = spi;
            spi = spi->next;
            free(temp);
        } else {
            prev_spi = spi;
            spi = spi->next;
        }
    }
    return ERROR;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 */
void closeSPI(void) {

}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @return [description]
 */
ErrorStatus setDefaulCfgSPI(uint8_t SPIMOD) {
    int statusVal = -1;
    struct spi_exp *spi;
    spi = lookup_exported_spi(SPIMOD);
    // one spi transfer for each byte
    if (spi == NULL) {
        printf("spi == NULL\r\n");
        return ERROR;
    }

    statusVal = ioctl(spi->spi_fd, SPI_IOC_WR_MODE, &(spi->mode));
    if (statusVal < 0) {
        perror("Could not set SPIMode (WR)...ioctl fail");
        return ERROR;
    }

    statusVal = ioctl(spi->spi_fd, SPI_IOC_RD_MODE, &(spi->mode));
    if (statusVal < 0) {
        perror("Could not set SPIMode (RD)...ioctl fail");
        return ERROR;
    }

    statusVal = ioctl(spi->spi_fd, SPI_IOC_WR_BITS_PER_WORD, &(spi->bitsPerWord));
    if (statusVal < 0) {
        perror("Could not set SPI bitsPerWord (WR)...ioctl fail");
        return ERROR;
    }

    statusVal = ioctl(spi->spi_fd, SPI_IOC_RD_BITS_PER_WORD, &(spi->bitsPerWord));
    if (statusVal < 0) {
        perror("Could not set SPI bitsPerWord(RD)...ioctl fail");
        return ERROR;
    }

    statusVal = ioctl(spi->spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &(spi->speed));
    if (statusVal < 0) {
        perror("Could not set SPI speed (WR)...ioctl fail");
        return ERROR;
    }

    statusVal = ioctl(spi->spi_fd, SPI_IOC_RD_MAX_SPEED_HZ, &(spi->speed));
    if (statusVal < 0) {
        perror("Could not set SPI speed (RD)...ioctl fail");
        return ERROR;
    }
    return SUCCESS;
}

/********************************API*******************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @param data [description]
 * @param length [description]
 * @return [description]
 */
ErrorStatus WriteReadSPI(uint8_t SPIMOD, uint16_t *data, int16_t length) {
    struct spi_ioc_transfer spid[length];
    int16_t ii = 0;
    int16_t retVal = -1;
    struct spi_exp *spi;
    spi = lookup_exported_spi(SPIMOD);
    // one spi transfer for each byte
    if (spi == NULL) {
        printf("spi == NULL\r\n");
        return ERROR;
    }
    for (ii = 0; ii < length; ii++) {
        spid[ii].tx_buf = (uint32_t) (data + ii); // transmit from "data"
        spid[ii].rx_buf = (uint32_t) (data + ii); // receive into "data"
        spid[ii].len = sizeof (*(data + ii));
        spid[ii].delay_usecs = 0;
        spid[ii].speed_hz = spi->speed;
        spid[ii].bits_per_word = spi->bitsPerWord;
        spid[ii].cs_change = 0;
    }

    retVal = ioctl(spi->spi_fd, SPI_IOC_MESSAGE(length), &spid);
    if (retVal < 0) {
        printf("Failed SPI RW data\r\n");
        return ERROR;
    }
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @param SPEED [description]
 *
 * @return [description]
 */
ErrorStatus setSpeedSPI(uint8_t SPIMOD, uint32_t SPEED) {
    //int16_t ii = 0;
    struct spi_exp *spi;
    spi = lookup_exported_spi(SPIMOD);
    // one spi transfer for each byte
    if (spi == NULL) {
        printf("spi == NULL\r\n");
        return ERROR;
    }
    spi->speed = SPEED;
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param SPIMOD [description]
 * @param BITSPERWORD [description]
 *
 * @return [description]
 */
ErrorStatus setBitsPerWordSPI(uint8_t SPIMOD, uint32_t BITSPERWORD) {
    //int16_t ii = 0;
    struct spi_exp *spi;
    spi = lookup_exported_spi(SPIMOD);
    // one spi transfer for each byte
    if (spi == NULL) {
        printf("spi == NULL\r\n");
        return ERROR;
    }
    spi->bitsPerWord = BITSPERWORD;
    return SUCCESS;
}
