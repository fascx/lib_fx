/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"



#define KEYLEN 32

int8_t OCP_DIR[32];
int16_t uart_initialized[nUARTDEV] = {1, 0, 0, 1, 0, 0};

/******************************************************************************/
struct uart_exp {
    int16_t MODULE;
    int16_t UART;
    int16_t uart_fd;
    struct uart_exp *next;
};
struct uart_exp *exported_uart = NULL;
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param UARTMOD [description]
 * @return [description]
 */
struct uart_exp *lookup_exported_uart(uint8_t UARTMOD) {
    struct uart_exp *uart = exported_uart;

    while (uart != NULL) {
        if (uart->UART == (UARTMOD)) {
            return uart;
        }
        uart = uart->next;
    }
    return NULL; /* standard for pointers */
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param UARTMOD [description]
 * @return [description]
 */
ErrorStatus load_dev_uart(uint8_t UARTMOD) {
    int16_t status = 0;
    switch (UARTMOD) {
        case 1:
            if (!uart_initialized[UARTMOD] && load_device_tree((char *) UART1_CAPE)) {
                uart_initialized[UARTMOD] = 1;
                status = 1;
            }
            break;
        case 2:
            if (!uart_initialized[UARTMOD] && load_device_tree((char *) UART2_CAPE)) {
                uart_initialized[UARTMOD] = 1;
                status = 1;
            }
            break;
        case 4:
            if (!uart_initialized[UARTMOD] && load_device_tree((char *) UART4_CAPE)) {
                uart_initialized[UARTMOD] = 1;
                status = 1;
            }
            break;
        case 5:
            if (!uart_initialized[UARTMOD] && load_device_tree((char *) UART5_CAPE)) {
                uart_initialized[UARTMOD] = 1;
                status = 1;
            }
            break;
        default:
            printf("ERROR UART CAPE DOES NOT EXIST\r\n");
            return ERROR;
    }
    if (status == 1) {
        build_path((char *) "/sys/devices", "ocp", (char *) OCP_DIR, sizeof (OCP_DIR));
        return SUCCESS;
    }
    return ERROR;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param UARTMOD [description]
 * @return [description]
 */
ErrorStatus LoadUART(int16_t UARTMOD) {
    int8_t *uart_path_p = NULL;
    int16_t uart_fd;
    struct uart_exp *new_uart = NULL, *uart = NULL;
    /*1)load primary capes ans key*/
    switch (UARTMOD) {
        case 1:
            uart_path_p = (int8_t *) UART1_PATH;
            break;
        case 2:
            uart_path_p = (int8_t *) UART2_PATH;
            break;
        case 4:
            uart_path_p = (int8_t *) UART4_PATH;
            break;
        case 5:

            uart_path_p = (int8_t *) UART5_PATH;
            break;
        default:
            printf("ERROR UART PATH DOES NOT EXIST\r\n");
            return ERROR;
    }

    /*2)load cape*/
    /*-------------------------------------------------------------------------*/
    if (!uart_initialized[UARTMOD]) {
        load_dev_uart(UARTMOD);
    }
    /*-------------------------------------------------------------------------*/
    /*3)create path*/
    /*-------------------------------------------------------------------------*/
    /*-------------------------------------------------------------------------*/
    /*4)open path*/
    /*-------------------------------------------------------------------------*/
    if ((uart_fd = open((char *) uart_path_p, O_RDWR | O_NOCTTY | O_NDELAY)) < 0) {
        //error, close already opened period_fd.
        printf("E uart_fd\r\n");
        return ERROR;
    }
    new_uart = malloc(sizeof (struct uart_exp));
    if (new_uart == 0) {
        printf("E malloc\r\n");
        return ERROR; // out of memory
    }
    new_uart->UART = UARTMOD;
    new_uart->uart_fd = uart_fd;
    new_uart->next = NULL;
    if (exported_uart == NULL) {
        //         create new list
        exported_uart = new_uart;
        printf("KEY4\r\n");
        //return ERROR;
    } else {
        // add to end of existing list
        uart = exported_uart;
        while (uart->next != NULL)
            uart = uart->next;
        uart->next = new_uart;
    }
    printf("KEY6\r\n");
    /*-------------------------------------------------------------------------*/
    return SUCCESS;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param UARTELM [description]
 * @return [description]
 */
ErrorStatus uart_disable(int16_t UARTELM) {
    struct uart_exp *uart, *temp, *prev_uart = NULL;
    // remove from list
    uart = exported_uart;
    while (uart != NULL) {
        if (uart->UART == (UARTELM)) {
            //close the fd
            close(uart->uart_fd);
            if (prev_uart == NULL) {
                exported_uart = uart->next;
                prev_uart = uart;
            } else {
                prev_uart->next = uart->next;
            }

            temp = uart;
            uart = uart->next;
            free(temp);
        } else {
            prev_uart = uart;
            uart = uart->next;
        }
    }
    return SUCCESS;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 */
void closeUART(void) {
    while (exported_uart != NULL) {
        uart_disable(exported_uart->UART);
    }
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param serial_fd [description]
 * @param data [description]
 * @param size [description]
 * @param timeout_usec [description]
 * @return [description]
 */
int serialRead(int serial_fd, char *data, int size, int timeout_usec) {
    fd_set fds;
    struct timeval timeout;
    int count = ERROR;
    int ret;
    int n;

    do {

        FD_ZERO(&fds);
        FD_SET(serial_fd, &fds);

        timeout.tv_sec = 0;
        timeout.tv_usec = timeout_usec;

        ret = select(FD_SETSIZE, &fds, NULL, NULL, &timeout);

        if (ret == 1) {

            n = read(serial_fd, &data[count], size - count);

            count += n;

            data[count] = 0;
        }

    } while (count < size && ret == 1);

    return count;
}

/********************************API*******************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param UARTMOD [description]
 * @param int [description]
 *
 * @return [description]
 */
ErrorStatus setSpeedUART(const uint8_t UARTMOD, const unsigned int Bauds) {
    struct termios serial_op; // Structure with the device's serial_op
    struct uart_exp *uart;
    uart = lookup_exported_uart(UARTMOD);
    if (uart == NULL) {
        printf("uart == NULL\r\n");
        return ERROR;
    }
    fcntl(uart->uart_fd, F_SETFL, FNDELAY);

    tcgetattr(uart->uart_fd, &serial_op);
    bzero(&serial_op, sizeof (serial_op));
    speed_t Speed;
    switch (Bauds) {
        case 110: Speed = B110;
            break;
        case 300: Speed = B300;
            break;
        case 600: Speed = B600;
            break;
        case 1200: Speed = B1200;
            break;
        case 2400: Speed = B2400;
            break;
        case 4800: Speed = B4800;
            break;
        case 9600: Speed = B9600;
            break;
        case 19200: Speed = B19200;
            break;
        case 38400: Speed = B38400;
            break;
        case 57600: Speed = B57600;
            break;
        case 115200: Speed = B115200;
            break;
        default: return ERROR;
    }
    serial_op.c_cflag = CBAUD | CS8 | CLOCAL | CREAD;
    serial_op.c_iflag = IGNPAR;
    serial_op.c_oflag = 0;
    serial_op.c_lflag = 0;
    serial_op.c_cc[VMIN] = 1;
    serial_op.c_cc[VTIME] = 0;

    cfsetispeed(&serial_op, Speed);
    cfsetospeed(&serial_op, Speed);


    if (tcflush(uart->uart_fd, TCIFLUSH) == -1) {
        return ERROR;
    }

    if (tcflush(uart->uart_fd, TCOFLUSH) == -1) {
        return ERROR;
    }

    if (tcsetattr(uart->uart_fd, TCSANOW, &serial_op) == -1) {
        return ERROR;
    }

    return SUCCESS;//(uart->uart_fd);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param UARTMOD [description]
 * @param Buffer [description]
 * @param length [description]
 * @param counts [description]
 * @return [description]
 */
int readUART(const uint8_t UARTMOD, void *Buffer, int length, int counts) {
    int readedBytes = 0;
    struct uart_exp *uart;
    uart = lookup_exported_uart(UARTMOD);
    if (uart == NULL) {
        printf("uart == NULL\r\n");
        return ERROR;
    }

    while ((counts--) > 0) {
        char *Ptr = (char *) Buffer + readedBytes;
        int Ret = serialRead(uart->uart_fd, (char *) Ptr, length - readedBytes, 100000);
        if (Ret == ERROR) {
            return ERROR;
        }
        if (Ret > 0) {
            readedBytes += Ret;
            if (readedBytes >= length) {
                return readedBytes;
            }
        }
    }
    return ERROR;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param UARTMOD [description]
 * @param Buffer [description]
 * @param length [description]
 * @return [description]
 */
ErrorStatus writeUART(const uint8_t UARTMOD, char *Buffer, int length) {
    struct uart_exp *uart;
    uart = lookup_exported_uart(UARTMOD);
    if (uart == NULL) {
        printf("uart == NULL\r\n");
        return ERROR;
    }

    if (write(uart->uart_fd, Buffer, length) != (ssize_t) length) // Write data
        return ERROR;
    return SUCCESS;

}
