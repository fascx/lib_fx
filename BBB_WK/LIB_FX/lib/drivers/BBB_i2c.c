/**
 ******************************************************************************
 * @file
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB.h"


#define KEYLEN 32

int8_t OCP_DIR[32];
int16_t i2c_initialized[nI2CDEV] = {0, 0 , 0};

/******************************************************************************/
struct i2c_exp
{
    int16_t MODULE;
    int16_t I2C;
    int16_t i2c_fd;
    struct i2c_exp *next;
};
struct i2c_exp *exported_i2c = NULL;
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @return [description]
 */
struct i2c_exp *lookup_exported_i2c(uint8_t I2CMOD)
{
    struct i2c_exp *i2c = exported_i2c;

    while (i2c != NULL)
    {
        if (i2c->I2C == (I2CMOD))
        {
            return i2c;
        }
        i2c = i2c->next;
    }
    return NULL; /* standard for pointers */
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @return [description]
 */
ErrorStatus load_dev_i2c(uint8_t I2CMOD)
{
    int16_t status = 0;
    if (I2CMOD == 2)
    {
        printf("i2c2\r\n");
        //
        if (!i2c_initialized[0] && load_device_tree((char *) I2C2_CAPE))
        {
            printf("i2c2:ok\r\n");
            i2c_initialized[0] = 1;
            status = 1;
        }
    }
    else if (I2CMOD == 1)
    {
        printf("i2c1:ok\r\n");
        if (!i2c_initialized[1])
        {
            printf("i2c1:ok\r\n");
            i2c_initialized[1] = 1;
            status = 1;
        }
    }
    else
    {
        printf("ERROR I2C CAPE DOES NOT EXIST\r\n");
        return ERROR;
    }
    if (status == 1)
    {
        build_path((char *) "/sys/devices", "ocp", (char *) OCP_DIR, sizeof (OCP_DIR));
        return SUCCESS;
    }
    printf("ERROR I2C CAPE CAN'T BE LOAD\r\n");
    return ERROR;
}
/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @return [description]
 */
ErrorStatus LoadI2C(int16_t I2CMOD)
{
    int8_t *i2c_path_p = NULL;
    int16_t i2c_fd;
    struct i2c_exp *new_i2c = NULL, *i2c = NULL;
    /*1)load primary capes ans key*/
    /*2)load cape*/
    printf("i2cmod :%d\n", I2CMOD );
    /*-------------------------------------------------------------------------*/
    if (!i2c_initialized[I2CMOD])
    {
        load_dev_i2c(I2CMOD);
    }
    /*-------------------------------------------------------------------------*/
    /*3)create path*/
    /*-------------------------------------------------------------------------*/
    if (I2CMOD == 2)
    {
        i2c_path_p = (int8_t *) I2C2_PATH;
    }
    else if (I2CMOD == 1)
    {
        i2c_path_p = (int8_t *) I2C1_PATH;
    }
    else
    {
        printf("ERROR I2C #\r\n");
        return ERROR;
    }
    printf("test %s \n", i2c_path_p );
    /*-------------------------------------------------------------------------*/
    /*4)open path*/
    /*-------------------------------------------------------------------------*/
    if ((i2c_fd = open((char *) i2c_path_p, O_RDWR)) < 0)
    {
        //error, close already opened period_fd.
        printf("E i2c_fd\r\n");
        return ERROR;
    }
    /*-------------------------------------------------------------------------*/
    /*insert elemnets in list*/
    /*-------------------------------------------------------------------------*/
    new_i2c = malloc(sizeof (struct i2c_exp));
    if (new_i2c == 0)
    {
        printf("E malloc\r\n");
        return ERROR; // out of memory
    }

    new_i2c->I2C = I2CMOD;
    new_i2c->i2c_fd = i2c_fd;
    new_i2c->next = NULL;
    if (exported_i2c == NULL)
    {
        exported_i2c = new_i2c;
        return SUCCESS;
    }
    else
    {
        i2c = exported_i2c;
        while (i2c->next != NULL)
            i2c = i2c->next;
        i2c->next = new_i2c;
    }
    printf("KEY\r\n");
    /*-------------------------------------------------------------------------*/
    return new_i2c->i2c_fd;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CELM [description]
 * @return [description]
 */
ErrorStatus i2c_disable(int16_t I2CELM)
{
    struct i2c_exp *i2c, *temp, *prev_i2c = NULL;
    // remove from list
    i2c = exported_i2c;
    while (i2c != NULL)
    {
        if (i2c->I2C == (I2CELM))
        {
            close(i2c->i2c_fd);
            if (prev_i2c == NULL)
            {
                exported_i2c = i2c->next;
                prev_i2c = i2c;
            }
            else
            {
                prev_i2c->next = i2c->next;
            }

            temp = i2c;
            i2c = i2c->next;
            free(temp);
        }
        else
        {
            prev_i2c = i2c;
            i2c = i2c->next;
        }
    }
    return SUCCESS;
}

/******************************************************************************/

/**
 * @brief [brief description]
 * @details [long description]
 */
void closeI2C(void)
{
    while (exported_i2c != NULL)
    {
        i2c_disable(exported_i2c->I2C);
    }
}
/********************************API*******************************************/

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param data [description]
 * @param r [description]
 * @param r [description]
 * @param a [description]
 * @return [description]
 */
int8_t i2cReadByte(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t *data)
{
    return i2cReadBytes(I2CMOD, devAddr, regAddr, data, 1);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param data [description]
 * @param length [description]
 * @return [description]
 */
int8_t i2cReadBytes(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t *data, uint8_t length)
{
    int8_t count = 0;
    struct i2c_exp *i2c;

    i2c = lookup_exported_i2c(I2CMOD);

    if (i2c == NULL)
    {
        printf("i2c == NULL\r\n");
        return ERROR;
    }
    //printf("r_fd:%d\r\n", i2c->i2c_fd);
    if (ioctl(i2c->i2c_fd, I2C_SLAVE, devAddr) < 0)
    {
        printf("Failed to select device\n");
        return ERROR;
    }
    if (write(i2c->i2c_fd, &regAddr, 1) != 1)
    {
        printf("Failed to i2cWrite reg\n");
        return ERROR;
    }
    count = read(i2c->i2c_fd, data, length);
    if (count < length)
    {
        printf("Failed to i2cRead device(%d)\n", count);
        return ERROR;
    }
    return count;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param data [description]
 * @param r [description]
 * @param r [description]
 * @param a [description]
 * @return [description]
 */
ErrorStatus i2cWriteByte(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t data)
{
    return i2cWriteBytes(I2CMOD, devAddr, regAddr, &data, 1);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param data [description]
 * @param length [description]
 * @return [description]
 */
ErrorStatus i2cWriteBytes(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t *data, uint8_t length)
{
    int8_t count = 0;
    uint8_t buf[128];
    struct i2c_exp *i2c;

    i2c = lookup_exported_i2c(I2CMOD);

    if (i2c == NULL)
    {
        printf("i2c == NULL\r\n");
        return ERROR;
    }

    if (length > 127)
    {
        printf("Byte i2cWrite count (%d) > 127\n", length);
        return ERROR;
    }

    if (ioctl(i2c->i2c_fd, I2C_SLAVE, devAddr) < 0)
    {
        printf("Failed to select device\n");
        return ERROR;
    }
    buf[0] = regAddr;
    memcpy(buf + 1, data, length);
    count = write(i2c->i2c_fd, buf, length + 1);
    if (count < (length + 1))
    {
        printf("Failed to i2cWrite device(%d)\n", count);
        return ERROR;
    }
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param data [description]
 * @param r [description]
 * @param r [description]
 * @param a [description]
 * @return [description]
 */
ErrorStatus i2cWriteWord(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint16_t data)
{
    return i2cWriteWords(I2CMOD, devAddr, regAddr, &data, 1);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param data [description]
 * @param length [description]
 * @return [description]
 */
ErrorStatus i2cWriteWords(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint16_t *data, uint8_t length)
{
    int8_t count = 0;
    uint8_t buf[128];
    int i;
    struct i2c_exp *i2c;

    i2c = lookup_exported_i2c(I2CMOD);

    if (i2c == NULL)
    {
        printf("i2c == NULL\r\n");
        return ERROR;
    }
    if (ioctl(i2c->i2c_fd, I2C_SLAVE, devAddr) < 0)
    {
        printf("Failed to select device\n");
        return ERROR;
    }
    buf[0] = regAddr;
    for (i = 0; i < length; i++)
    {
        buf[i * 2 + 1] = data[i] >> 8;
        buf[i * 2 + 2] = data[i];
    }
    count = write(i2c->i2c_fd, buf, length * 2 + 1);
    if (count < (length * 2 + 1))
    {
        printf("Failed to i2cWrite device(%d)\n", count);
        return ERROR;
    }
    return SUCCESS;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param bitNum [description]
 * @param data [description]
 * @return [description]
 */
ErrorStatus i2cWriteBit(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t data)
{
    uint8_t b;
    i2cReadByte(I2CMOD, devAddr, regAddr, &b);
    b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
    return i2cWriteByte(I2CMOD, devAddr, regAddr, b);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param bitStart [description]
 * @param length [description]
 * @param data [description]
 * @return [description]
 */
ErrorStatus i2cWriteBits(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t data)
{
    uint8_t b;
    if (i2cReadByte(I2CMOD, devAddr, regAddr, &b) != 0)
    {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        data <<= (bitStart - length + 1);
        data &= mask;
        b &= ~(mask);
        b |= data;
        return i2cWriteByte(I2CMOD, devAddr, regAddr, b);
    }
    else
    {
        return ERROR;
    }
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param I2CMOD [description]
 * @param devAddr [description]
 * @param regAddr [description]
 * @param bitStart [description]
 * @param length [description]
 * @param data [description]
 * @return [description]
 */
int8_t i2cReadBits(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t *data)
{
    uint8_t count, b;
    if ((count = i2cReadByte(I2CMOD, devAddr, regAddr, &b)) != 0)
    {
        uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
        b &= mask;
        b >>= (bitStart - length + 1);
        *data = b;
    }
    return count;
}