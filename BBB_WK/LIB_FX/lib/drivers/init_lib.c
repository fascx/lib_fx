/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#include "BBB_DEF.h"


volatile void *MMAP_libfx = NULL; /*puntero al mapa de memoria*/
short LOADD_MMAP_libfx = FALSE; /*bandera para indicar si el mapa a sido cargado */
int fd_libfx = -1;
char ctrl_dir[35];

/**
 * @brief [brief description]
 * @details [long description]
 * @return [description]
 */
volatile void *Load_libfx(void) {
    if (!LOADD_MMAP_libfx) {

        if ((fd_libfx = open("/dev/mem", O_RDWR | O_SYNC)) == -1) {
            printf("/dev/mem could not be opened.\n");
            perror("open");
            exit(EXIT_FAILURE);
        } else {
            printf("/dev/mem opened.\n");
        }
        MMAP_libfx = mmap(0, PMMap_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd_libfx, PMMap_START_ADDR);
        if (MMAP_libfx == MAP_FAILED) {
            close(fd_libfx);
            perror("Unable to map PP");
            exit(EXIT_FAILURE);
        }
        LOADD_MMAP_libfx = TRUE;
    }
    return MMAP_libfx;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param d [description]
 * @param E [description]
 */
void Close_libfx(void) {
    if (munmap((void *) MMAP_libfx, PMMap_SIZE) == -1) {
        printf("Memory unmap failed.\n");
    }
    close(fd_libfx);
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param partial_path [description]
 * @param prefix [description]
 * @param full_path [description]
 * @param full_path_len [description]
 * @return [description]
 */
int build_path(char *partial_path, char *prefix, char *full_path, size_t full_path_len) {
    DIR *dp;
    struct dirent *ep;

    dp = opendir(partial_path);
    if (dp != NULL) {
        while ((ep = readdir(dp))) {
            // Enforce that the prefix must be the first part of the file
            char *found_string = strstr(ep->d_name, prefix);

            if (found_string != NULL && (ep->d_name - found_string) == 0) {
                snprintf(full_path, full_path_len, "%s/%s", partial_path, ep->d_name);
                printf("build_path :%s\r\n", full_path);
                (void) closedir(dp);
                return 1;
            }
        }
        (void) closedir(dp);
    } else {
        return 0;
    }

    return 0;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param name [description]
 * @return [description]
 */
int load_device_tree(char *name) {
    FILE *file = NULL;
    char slots[40];
    char line[256];

    build_path("/sys/devices", "bone_capemgr", ctrl_dir, sizeof (ctrl_dir));
    snprintf(slots, sizeof (slots), "%s/slots", ctrl_dir);

    file = fopen(slots, "r+");
    if (!file) {
        printf("ERROR load DT");
        return 0;
    }
    while (fgets(line, sizeof (line), file)) {
        //the device is already loaded, return 1
        if (strstr(line, name)) {
            fclose(file);
            return 1;
        }
    }

    //if the device isn't already loaded, load it, and return
    fprintf(file, name);
    fclose(file);

    //0.2 second delay

    nanosleep((struct timespec[]) {
        {0, 500000000}
    }, NULL);

    return 1;
}

/**
 * @brief [brief description]
 * @details [long description]
 *
 * @param name [description]
 * @return [description]
 */
int unload_device_tree(char *name) {
    FILE *file = NULL;
    char slots[40];
    char line[256];
    char *slot_line;

    snprintf(slots, sizeof (slots), "%s/slots", ctrl_dir);

    file = fopen(slots, "r+");
    if (!file) {
        printf("ERROR unload DT");
        return 0;
    }

    while (fgets(line, sizeof (line), file)) {
        //the device is loaded, let's unload it
        if (strstr(line, name)) {
            slot_line = strtok(line, ":");
            //remove leading spaces
            while (*slot_line == ' ')
                slot_line++;

            fprintf(file, "-%s", slot_line);
            fclose(file);
            return 1;
        }
    }

    //not loaded, close file
    fclose(file);

    return 0;
}
