/**
 ******************************************************************************
 * @file    BBB_DEF.h
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   This file contains all the functions prototypes for the ADC library.
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */

#ifndef BBB_H
#define BBB_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/i2c-dev.h>
#include <linux/spi/spidev.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <stddef.h>
#include <time.h>
#include <termios.h>
#include <dirent.h>

#include "init_lib.h"
#include  "hw_types.h"
#include  "map_AM335x.h"
#include  "pin_mux.h"
#include  "gpio.h"



//#define TRUE    1
//#define FALSE   0
/*ADC*/

#define ADC_CAPE "LIBFX_ADC"

/*UART*/

#define nUARTDEV 6
#define UART1_PATH "/dev/ttyO1"
#define UART2_PATH "/dev/ttyO2"
#define UART4_PATH "/dev/ttyO4"
#define UART5_PATH "/dev/ttyO5"
#define UART1_CAPE "LIBFX_UART1"
#define UART2_CAPE "LIBFX_UART2"
#define UART4_CAPE "LIBFX_UART4"
#define UART5_CAPE "LIBFX_UART5"

/*PWM*/
#define nPWMDEV 3
#define PWM_PIN_CAPE "LIBFX_PWM_"
#define PWM_CAPE "am33xx_pwm"

/*QEI*/
#define nQEIDEV 3
#define QEI0_PATH "48300000.epwmss/48300180.eqep"
#define QEI1_PATH "48302000.epwmss/48302180.eqep"
#define QEI2_PATH "48304000.epwmss/48304180.eqep"
#define QEI0_CAPE "LIBFX_BB_EQEP0"
#define QEI1_CAPE "LIBFX_BB_EQEP1"
#define QEI2_CAPE "LIBFX_BB_EQEP2"

/*I2C*/
#define nI2CDEV 3
//#define I2C0_PATH "/dev/i2c-0"
#define I2C1_PATH "/dev/i2c-1"
#define I2C2_PATH "/dev/i2c-2"
//#define I2C0_CAPE ""
#define I2C1_CAPE ""
#define I2C2_CAPE "BB-I2C1"
/*SPI*/
#define nSPIDEV 2
#define SPI0_PATH "/dev/spidev0.0"
#define SPI1_PATH "/dev/spidev1.0"
#define SPI0_CAPE "BB-SPIDEV0"  //LIBFX_SPIDEV0"
#define SPI1_CAPE "BB-SPIDEV1"  //"LIBFX_SPIDEV1"

/**
USER0 is the heartbeat indicator from the Linux kernel.
USER1 turns on when the microSD card is being accessed
USER2 is an activity indicator. It turns on when the kernel is not in the idle loop.
USER3 turns on when the onboard eMMC is being accessed.
 **/

#define USR0_LED (1<<21)
#define USR1_LED (1<<22)
#define USR2_LED (1<<23)
#define USR3_LED (1<<24)

/**********************************************************************/

#define INPUT                   (GPIO_OE_OUTPUT_DISABLED)
#define OUTPUT                  (GPIO_OE_OUTPUT_ENABLED)

/***********************************************************************/
#define P8_01   0
#define P8_02   1
#define P8_03   2
#define P8_04   3
#define P8_05   4
#define P8_06   5
#define P8_07   6
#define P8_08   7
#define P8_09   8
#define P8_10   9
#define P8_11   10
#define P8_12   11
#define P8_13   12
#define P8_14   13
#define P8_15   14
#define P8_16   15
#define P8_17   16
#define P8_18   17
#define P8_19   18
#define P8_20   19
#define P8_21   20
#define P8_22   21
#define P8_23   22
#define P8_24   23
#define P8_25   24
#define P8_26   25
#define P8_27   26
#define P8_28   27
#define P8_29   28
#define P8_30   29
#define P8_31   30
#define P8_32   31
#define P8_33   32
#define P8_34   33
#define P8_35   34
#define P8_36   35
#define P8_37   36
#define P8_38   37
#define P8_39   38
#define P8_40   39
#define P8_41   40
#define P8_42   41
#define P8_43   42
#define P8_44   43
#define P8_45   44
#define P8_46   45
#define P9_01   46
#define P9_02   47
#define P9_03   48
#define P9_04   49
#define P9_05   50
#define P9_06   51
#define P9_07   52
#define P9_08   53
#define P9_09   54
#define P9_10   55
#define P9_11   56
#define P9_12   57
#define P9_13   58
#define P9_14   59
#define P9_15   60
#define P9_16   61
#define P9_17   62
#define P9_18   63
#define P9_19   64
#define P9_20   65
#define P9_21   66
#define P9_22   67
#define P9_23   68
#define P9_24   69
#define P9_25   70
#define P9_26   71
#define P9_27   72
#define P9_28   73
#define P9_29   74
#define P9_30   75
#define P9_31   76
#define P9_32   77
#define P9_33   78
#define P9_34   79
#define P9_35   80
#define P9_36   81
#define P9_37   82
#define P9_38   83
#define P9_39   84






#ifdef __cplusplus
}
#endif

#endif  // end of _BBB_H definition*/


/************************ COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS *****END OF FILE****/
