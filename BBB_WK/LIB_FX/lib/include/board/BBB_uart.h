/*
 * File:   BBB_uart.h
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 7 de diciembre de 2013, 08:12 AM
 */

#ifndef BBB_UART_H
#define BBB_UART_H

#ifdef  __cplusplus
extern "C" {
#endif

#define UART1 1
#define UART2 2
#define UART4 4
#define UART5 5
struct uart_exp *lookup_exported_uart(uint8_t UARTMOD);
ErrorStatus load_dev_uart(uint8_t UARTMOD);
ErrorStatus LoadUART(int16_t UARTMOD);
ErrorStatus uart_disable(int16_t UARTELM);
void closeUART(void);
ErrorStatus setSpeedUART(const uint8_t UARTMOD, const unsigned int Bauds);
int readUART(const uint8_t UARTMOD, void *Buffer,  int length,  int counts) ;
ErrorStatus writeUART(const uint8_t UARTMOD, char *Buffer, int length) ;
#ifdef  __cplusplus
}
#endif

#endif  /* BBB_UART_H */
