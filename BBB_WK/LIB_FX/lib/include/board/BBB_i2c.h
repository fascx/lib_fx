/*
 * File:   BBB_i2c.h
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 3 de diciembre de 2013, 10:41 PM
 */

#ifndef BBB_I2C_H
#define BBB_I2C_H

#ifdef  __cplusplus
extern "C" {
#endif

#define I2C1 1
#define I2C2 2
ErrorStatus load_dev_i2c(uint8_t I2CMOD);
ErrorStatus LoadI2C(int16_t I2CMOD);
ErrorStatus i2c_disable(int16_t I2CELM);
void closeI2C(void);
int8_t i2cReadByte(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t *data);
int8_t i2cReadBytes(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t *data, uint8_t length);
ErrorStatus i2cWriteByte(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t data);
ErrorStatus i2cWriteBytes(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t *data, uint8_t length);
ErrorStatus i2cWriteWord(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint16_t data);
ErrorStatus i2cWriteWords(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint16_t *data, uint8_t length);
ErrorStatus i2cWriteBit(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t data);
ErrorStatus i2cWriteBits(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t data);
int8_t i2cReadBits(uint8_t I2CMOD, uint8_t devAddr, uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t *data);
#ifdef  __cplusplus
}
#endif

#endif  /* BBB_I2C_H */
