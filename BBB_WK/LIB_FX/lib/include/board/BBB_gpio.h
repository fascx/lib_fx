/* 
 * File:   BBB_gpio.h
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 27 de noviembre de 2013, 10:14 PM
 */

#ifndef BBB_GPIO_H
#define	BBB_GPIO_H

#ifdef	__cplusplus
extern "C" {
#endif



    /*
     Set a bit
    bit_fld |= (1 << n)
    Clear a bit
    bit_fld &= ~(1 << n)
    Toggle a bit
    bit_fld ^= (1 << n)
    Test a bit
    bit_fld & (1 << n)

     */
    ErrorStatus LoadGpioPin(uint16_t GPIOP, uint16_t DIR);
    ErrorStatus gpio_export(unsigned int gpio);
    ErrorStatus gpio_unexport(unsigned int gpio);
    void closeGpio(void);
    /*******************************gpio********************************************/
    ErrorStatus setGpioPinDir(uint16_t GPIOP, uint16_t DIR);
    BitStatus getGpioPinDir(uint16_t GPIOP);
    ErrorStatus setGpioPin(uint16_t GPIOP);
    ErrorStatus clearGpioPin(uint16_t GPIOP);
    BitStatus readGpioPin(uint16_t GPIOP);
    ErrorStatus writeGpioPin(uint16_t GPIOP, uint16_t VALUE);
    /*******************************gpio-getmux*************************************/
    // int16_t getGpioSlewRate(int16_t GPIOP);
    // int16_t getGpioRxEn(int16_t GPIOP);
    // int16_t getGpioPullType(int16_t GPIOP);
    // int16_t getGpioPullEn(int16_t GPIOP);
    // int16_t getGpioMuxMode(int16_t GPIOP);
#ifdef	__cplusplus
}
#endif

#endif	/* BBB_GPIO_H */

