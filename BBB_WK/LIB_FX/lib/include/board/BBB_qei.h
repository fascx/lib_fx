/*
 * File:   BBB_qei.h
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 2 de diciembre de 2013, 01:15 PM
 */

#ifndef BBB_QEI_H
#define BBB_QEI_H

#ifdef  __cplusplus
extern "C" {
#endif

#define QEI0 0
#define QEI1 1
#define QEI2 2

ErrorStatus load_dev_qei(int16_t QEIMOD);
ErrorStatus LoadQEI(int16_t QEIMOD);
ErrorStatus setFrecuencyQEI(int16_t QEIMOD, float freq);
ErrorStatus setModeQEI(int16_t QEIMOD, int16_t mode);
ErrorStatus setPositionQEI(int16_t QEIMOD, int32_t value);
ErrorStatus getPositionQEI(int16_t QEIMOD,int32_t *value);
ErrorStatus qei_disable(int16_t QEIMOD);
ErrorStatus setQEI(int16_t QEIMOD, int16_t enable);
ErrorStatus setEnableQEI(int16_t QEIMOD);
ErrorStatus setDisableQEI(int16_t QEIMOD);
void closeQEI(void);
#ifdef  __cplusplus
}
#endif

#endif  /* BBB_QEI_H */

