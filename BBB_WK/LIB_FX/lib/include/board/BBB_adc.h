/*
 * File:   BBB_adc.h
 * Author: Fredy Alexander Suárez Cárdenas
 *
 * Created on 2 de diciembre de 2013, 04:47 PM
 */

#ifndef BBB_ADC_H
#define BBB_ADC_H

#ifdef  __cplusplus
extern "C" {
#endif

#define AD0     0
#define AD1     1
#define AD2     2
#define AD3     3
#define AD4     4
#define AD5     5
#define AD6     6
#define AD7     7

#define AN0     0
#define AN1     1
#define AN2     2
#define AN3     3
#define AN4     4
#define AN5     5
#define AN6     6
#define AN7     7

ErrorStatus load_dev_adc(void);
ErrorStatus readADC(unsigned int ain, float *value);
ErrorStatus LoadADC(void);
void adc_disable(void);
void closeADC(void);
#ifdef  __cplusplus
}
#endif

#endif  /* BBB_ADC_H */

