/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#ifndef _AM33XX_H_
#define _AM33XX_H_

#ifdef __cplusplus
extern "C" {
#endif
  #include <stdint.h>
//---------------------------------------------------------------------------
// Common CPU Memory Map:
//
//-----------------------------
//Peripheral Memory Map
#define PMMap_START_ADDR 	0x44C00000 
#define PMMap_END_ADDR 		0x48FFFFFF 
#define PMMap_SIZE			(PMMap_END_ADDR - PMMap_START_ADDR)
//-----------------------------
// Control Module Memory Map:
#define CTRLMD_START_ADDR 	0x44E10000
#define CTRLMD_END_ADDR 	0x44E11FFF 
#define CTRLMD_SIZE			(CTRLMD_END_ADDR - CTRLMD_START_ADDR)

  
/** @brief Base address of AINTC memory mapped registers*/
#define AINTC_REGS			(0x48200000)

/** @brief Base addresses of GPIO memory mapped registers*/
#define GPIO_0_REGS			(0x44E07000)
#define GPIO_1_REGS			(0x4804C000)
#define GPIO_2_REGS			(0x481AC000)
#define GPIO_3_REGS			(0x481AE000)

/** @brief Base addresses of DMTIMER memory mapped registers*/
#define DMTIMER_0_REGS			(0x44E05000)
#define DMTIMER_1_REGS			(0x44E31000)
#define DMTIMER_2_REGS			(0x48040000)
#define DMTIMER_3_REGS			(0x48042000)
#define DMTIMER_4_REGS			(0x48044000)
#define DMTIMER_5_REGS			(0x48046000)
#define DMTIMER_6_REGS			(0x48048000)
#define DMTIMER_7_REGS			(0x4804A000)

/** @brief Base addresses of RTC memory mapped registers*/
#define RTC_0_REGS			  (0x44E3E000)

/** @brief Base addresses of PRCM memory mapped registers*/
#define PRCM_REGS			(0x44E00000)
#define CM_PER_REGS			(PRCM_REGS + 0)
#define CM_WKUP_REGS			(PRCM_REGS + 0x400)
#define PRM_PER_REGS			(PRCM_REGS + 0xC00)
#define PRM_WKUP_REGS			(PRCM_REGS + 0xD00)

/** @brief Base address of Control Module memory mapped registers*/
#define CONTROL_REGS			(0x44E10000)

/** @brief Base address of TSC ADC memory mapped registers*/
#define ADC_TSC_0_REGS			0x44E0D000

/** @brief Base addresses of PWMSS memory mapped registers*/
#define PWMSS0_REGS			(0x48300000)
#define PWMSS1_REGS			(0x48302000)
#define PWMSS2_REGS			(0x48304000)

#define ECAP_REGS			(0x00000100)
#define EQEP_REGS			(0x00000180)
#define EPWM_REGS			(0x00000200)

#define ECAP_0_REGS			(PWMSS0_REGS + ECAP_REGS)
#define ECAP_1_REGS			(PWMSS1_REGS + ECAP_REGS)
#define ECAP_2_REGS			(PWMSS2_REGS + ECAP_REGS)

#define EQEP_0_REGS			(PWMSS0_REGS + EQEP_REGS)
#define EQEP_1_REGS			(PWMSS1_REGS + EQEP_REGS)
#define EQEP_2_REGS			(PWMSS2_REGS + EQEP_REGS) 

#define EPWM_0_REGS			(PWMSS0_REGS + EPWM_REGS)
#define EPWM_1_REGS			(PWMSS1_REGS + EPWM_REGS)
#define EPWM_2_REGS			(PWMSS2_REGS + EPWM_REGS)

 
#ifdef __cplusplus
}
#endif

#endif 
