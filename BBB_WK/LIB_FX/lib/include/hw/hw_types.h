/**
 ******************************************************************************
 * @file
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */

#ifndef _HW_TYPES_H_
#define _HW_TYPES_H_

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

#ifndef NULL
#define NULL ((void*) 0)
#endif

//#define TRUE    1
//#define FALSE   0
//#define OK      1
//#define FAIL    -1
//#define SUCCESS 0
//#define FAILURE -1

typedef enum
{
    FALSE = 0,
    TRUE = !FALSE
} BoolStatus;

typedef enum
{
    LOW = 0,
    HIGH = !LOW
} BitStatus;
/**
 *
 */
typedef enum
{
    RESET = 0,
    SET = !RESET
} FlagStatus;

/**
 *
 */
typedef enum
{
    ERROR = 0,
    SUCCESS = !ERROR
} ErrorStatus;


//#define HIGH    1
//#define LOW     0

#define SET_BIT(REG, BIT)     ((REG) |= (BIT))

#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))

#define READ_BIT(REG, BIT)    ((REG) & (BIT))

#define CLEAR_REG(REG)        ((REG) = (0x0))

#define WRITE_REG(REG, VAL)   ((REG) = (VAL))

#define READ_REG(REG)         ((REG))



#define BIT(x) (1 << (x))
#define HWREG(x)                                                              \
    (*((volatile unsigned long *)(x)))
#define HWREGH(x)                                                             \
    (*((volatile unsigned short *)(x)))
#define HWREGB(x)                                                             \
    (*((volatile unsigned char *)(x)))
#define HWREGBITW(x, b)                                                       \
    HWREG(((unsigned long)(x) & 0xF0000000) | 0x02000000 |                \
          (((unsigned long)(x) & 0x000FFFFF) << 5) | ((b) << 2))
#define HWREGBITH(x, b)                                                       \
    HWREGH(((unsigned long)(x) & 0xF0000000) | 0x02000000 |               \
           (((unsigned long)(x) & 0x000FFFFF) << 5) | ((b) << 2))
#define HWREGBITB(x, b)                                                       \
    HWREGB(((unsigned long)(x) & 0xF0000000) | 0x02000000 |               \
           (((unsigned long)(x) & 0x000FFFFF) << 5) | ((b) << 2))

/*#ifndef DATA_TYPES
#define DATA_TYPES
typedef int                int16_t;
typedef long               int32_t;
typedef long long          int64_t;
#ifndef STD_TYPES
typedef unsigned int       uint16_t;
typedef unsigned long      uint32_t;
typedef unsigned long long uint64_t;
#endif
typedef float              float32_t;
typedef long double        float64_t;
#endif
*/
#endif // __HW_TYPES_H__
