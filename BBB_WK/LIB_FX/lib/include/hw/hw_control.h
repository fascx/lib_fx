/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#ifndef _HW_CONTROL_H_
#define _HW_CONTROL_H_

#ifdef __cplusplus
extern "C" {
#endif

#define CONTROL_REVISION			(0x0)
#define CONTROL_HWINFO				(0x4)
#define CONTROL_SYSCONFIG			(0x10)
#define CONTROL_STATUS				(0x40)

/* PWMSS_CTRL */
#define CONTROL_PWMSS_CTRL                      (0x664)

#define CONTROL_PWMSS_CTRL_PWMSS0_TBCLKEN       (0x00000001u)
#define CONTROL_PWMSS_CTRL_PWMSS0_TBCLKEN_BIT   (0x00000000u)

#define CONTROL_PWMSS_CTRL_PWMMS1_TBCLKEN       (0x00000002u)
#define CONTROL_PWMSS_CTRL_PWMMS1_TBCLKEN_BIT   (0x00000001u)

#define CONTROL_PWMSS_CTRL_PWMSS2_TBCLKEN       (0x00000004u)
#define CONTROL_PWMSS_CTRL_PWMSS2_TBCLKEN_BIT   (0x00000002u)

#ifdef __cplusplus
}
#endif

#endif
