/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#ifndef _HW_GPIO_H_
#define _HW_GPIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#define GPIO_REVISION		(0x0)
#define GPIO_SYSCONFIG		(0x10)
#define GPIO_IRQSTATUS_RAW(n)	(0x24 + (n * 4))
#define GPIO_IRQSTATUS(n)	(0x2C + (n * 4))
#define GPIO_IRQSTATUS_SET(n)	(0x34 + (n * 4))
#define GPIO_IRQSTATUS_CLR(n)	(0x3C + (n * 4))
#define GPIO_IRQWAKEN(n)	(0x44 + (n * 4))
#define GPIO_SYSSTATUS		(0x114)
#define GPIO_CTRL		(0x130)
#define GPIO_OE			(0x134)
#define GPIO_DATAIN		(0x138)
#define GPIO_DATAOUT		(0x13C)
#define GPIO_LEVELDETECT(n)	(0x140 + (n * 4))
#define GPIO_RISINGDETECT	(0x148)
#define GPIO_FALLINGDETECT	(0x14C)
#define GPIO_DEBOUNCENABLE	(0x150)
#define GPIO_DEBOUNCINGTIME	(0x154)
#define GPIO_CLEARDATAOUT	(0x190)
#define GPIO_SETDATAOUT		(0x194)


/*GPIO_OE*/
#define GPIO_OE_OUTPUT_DISABLED   (0x1u)
#define GPIO_OE_OUTPUT_ENABLED    (0x0u)

#ifdef __cplusplus
}
#endif

#endif

