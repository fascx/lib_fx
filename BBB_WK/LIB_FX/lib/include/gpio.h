/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#ifndef  _GPIO_H_
#define  _GPIO_H_
#include "map_AM335x.h"
#include "hw_gpio.h"
#ifdef __cplusplus
extern "C" {
#endif

#define GPIO_DIR_INPUT                   (GPIO_OE_OUTPUT_DISABLED)
#define GPIO_DIR_OUTPUT                  (GPIO_OE_OUTPUT_ENABLED)

#define GPIO_PIN_LOW                     (LOW)
#define GPIO_PIN_HIGH                    (HIGH)

BitStatus ReadGPIOPin(uint32_t baseAdd, uint16_t pinNumber);
void WriteGPIOPin(uint32_t baseAdd, uint16_t pinNumber, uint16_t pinValue);
BitStatus GetGPIODirMode(uint32_t baseAdd, uint16_t pinNumber);
void SetGPIODirMode(uint32_t baseAdd, uint16_t pinNumber, uint16_t pinDirection);


#ifdef __cplusplus
}
#endif
#endif
