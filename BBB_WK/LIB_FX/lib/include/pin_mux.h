/**
 ******************************************************************************
 * @file    
 * @author  FREDY ALEXANDER SUÁREZ CÁRDENAS
 * @version V
 * @date    05-March-2012
 * @brief   
 *
 ******************************************************************************
 * @attention
 *
 *  COPYRIGHT 2013 FREDY ALEXANDER SUÁREZ CÁRDENAS
 *
 *
 ******************************************************************************
 */
#ifndef _PIN_MUX_H_
#define _PIN_MUX_H_

#include "hw_control.h"

#ifdef __cplusplus
extern "C" {
#endif

#define GPIO0_0                 (0x0948)
#define GPIO0_1                 (0x094c)
#define GPIO0_2                 (0x0950)
#define GPIO0_3                 (0x0954)
#define GPIO0_4                 (0x0958)
#define GPIO0_5                 (0x095c)
#define GPIO0_6                 (0x0960)
#define GPIO0_7                 (0x0964)
#define GPIO0_8                 (0x08d0)
#define GPIO0_9                 (0x08d4)
#define GPIO0_10                (0x08d8)
#define GPIO0_11                (0x08dc)
#define GPIO0_12                (0x0978)
#define GPIO0_13                (0x097c)
#define GPIO0_14                (0x0980)
#define GPIO0_15                (0x0984)
#define GPIO0_16                (0x091c)
#define GPIO0_17                (0x0920)
#define GPIO0_18                (0x0a1c)
#define GPIO0_19                (0x09b0)
#define GPIO0_20                (0x09b4)
#define GPIO0_21                (0x0924)
#define GPIO0_22                (0x0820)
#define GPIO0_23                (0x0824)
#define GPIO0_26                (0x0828)
#define GPIO0_27                (0x082c)
#define GPIO0_28                (0x0928)
#define GPIO0_29                (0x0944)
#define GPIO0_30                (0x0870)
#define GPIO0_31                (0x0874)

#define GPIO1_0                 (0x0800)
#define GPIO1_1                 (0x0804)
#define GPIO1_2                 (0x0808)
#define GPIO1_3                 (0x080c)
#define GPIO1_4                 (0x0810)
#define GPIO1_5                 (0x0814)
#define GPIO1_6                 (0x0818)
#define GPIO1_7                 (0x081c)
#define GPIO1_8                 (0x0968)
#define GPIO1_9                 (0x096c)
#define GPIO1_10                (0x0970)
#define GPIO1_11                (0x0974)
#define GPIO1_12                (0x0830)
#define GPIO1_13                (0x0834)
#define GPIO1_14                (0x0838)
#define GPIO1_15                (0x083c)
#define GPIO1_16                (0x0840)
#define GPIO1_17                (0x0844)
#define GPIO1_18                (0x0848)
#define GPIO1_19                (0x084c)
#define GPIO1_20                (0x0850)
#define GPIO1_21                (0x0854)
#define GPIO1_22                (0x0858)
#define GPIO1_23                (0x085c)
#define GPIO1_24                (0x0860)
#define GPIO1_25                (0x0864)
#define GPIO1_26                (0x0868)
#define GPIO1_27                (0x086c)
#define GPIO1_28                (0x0878)
#define GPIO1_29                (0x087c)
#define GPIO1_30                (0x0880)
#define GPIO1_31                (0x0884)

#define GPIO2_0                 (0x0888)
#define GPIO2_1                 (0x088c)
#define GPIO2_2                 (0x0890)
#define GPIO2_3                 (0x0894)
#define GPIO2_4                 (0x0898)
#define GPIO2_5                 (0x089c)
#define GPIO2_6                 (0x08a0)
#define GPIO2_7                 (0x08a4)
#define GPIO2_8                 (0x08a8)
#define GPIO2_9                 (0x08ac)
#define GPIO2_10                (0x08b0)
#define GPIO2_11                (0x08b4)
#define GPIO2_12                (0x08b8)
#define GPIO2_13                (0x08bc)
#define GPIO2_14                (0x08c0)
#define GPIO2_15                (0x08c4)
#define GPIO2_16                (0x08c8)
#define GPIO2_17                (0x08cc)
#define GPIO2_18                (0x0934)
#define GPIO2_19                (0x0938)
#define GPIO2_20                (0x093c)
#define GPIO2_21                (0x0940)
#define GPIO2_22                (0x08e0)
#define GPIO2_23                (0x08e4)
#define GPIO2_24                (0x08e8)
#define GPIO2_25                (0x08ec)
#define GPIO2_26                (0x08f0)
#define GPIO2_27                (0x08f4)
#define GPIO2_28                (0x08f8)
#define GPIO2_29                (0x08fc)
#define GPIO2_30                (0x0900)
#define GPIO2_31                (0x0904)

#define GPIO3_0                 (0x0908)
#define GPIO3_1                 (0x090c)
#define GPIO3_2                 (0x0910)
#define GPIO3_3                 (0x0914)
#define GPIO3_4                 (0x0918)
#define GPIO3_5                 (0x0988)
#define GPIO3_6                 (0x098c)
#define GPIO3_7                 (0x09e4)
#define GPIO3_8                 (0x09e8)
#define GPIO3_9                 (0x092c)
#define GPIO3_10                (0x0930)
#define GPIO3_13                (0x0a34)
#define GPIO3_14                (0x0990)
#define GPIO3_15                (0x0994)
#define GPIO3_16                (0x0998)
#define GPIO3_17                (0x099c)
#define GPIO3_18                (0x09a0)
#define GPIO3_19                (0x09a4)
#define GPIO3_20                (0x09a8)
#define GPIO3_21                (0x09ac)

/*linux pin*/
#define PIN_0			0x44e10800
#define PIN_1			0x44e10804
#define PIN_2			0x44e10808
#define PIN_3			0x44e1080c
#define PIN_4			0x44e10810
#define PIN_5			0x44e10814
#define PIN_6			0x44e10818
#define PIN_7			0x44e1081c
#define PIN_8			0x44e10820
#define PIN_9			0x44e10824
#define PIN_10			0x44e10828
#define PIN_11			0x44e1082c
#define PIN_12			0x44e10830
#define PIN_13			0x44e10834
#define PIN_14			0x44e10838
#define PIN_15			0x44e1083c
#define PIN_16			0x44e10840
#define PIN_17			0x44e10844
#define PIN_18			0x44e10848
#define PIN_19			0x44e1084c
#define PIN_20			0x44e10850
#define PIN_21			0x44e10854
#define PIN_22			0x44e10858
#define PIN_23			0x44e1085c
#define PIN_24			0x44e10860
#define PIN_25			0x44e10864
#define PIN_26			0x44e10868
#define PIN_27			0x44e1086c
#define PIN_28			0x44e10870
#define PIN_29			0x44e10874
#define PIN_30			0x44e10878
#define PIN_31			0x44e1087c

#define PIN_32			0x44e10880
#define PIN_33			0x44e10884
#define PIN_34			0x44e10888
#define PIN_35			0x44e1088c
#define PIN_36			0x44e10890
#define PIN_37			0x44e10894
#define PIN_38			0x44e10898
#define PIN_39			0x44e1089c
#define PIN_40			0x44e108a0
#define PIN_41			0x44e108a4
#define PIN_42			0x44e108a8
#define PIN_43			0x44e108ac
#define PIN_44			0x44e108b0
#define PIN_45			0x44e108b4
#define PIN_46			0x44e108b8
#define PIN_47			0x44e108bc
#define PIN_48			0x44e108c0
#define PIN_49			0x44e108c4
#define PIN_50			0x44e108c8
#define PIN_51			0x44e108cc
#define PIN_52			0x44e108d0
#define PIN_53			0x44e108d4
#define PIN_54			0x44e108d8
#define PIN_55			0x44e108dc
#define PIN_56			0x44e108e0
#define PIN_57			0x44e108e4
#define PIN_58			0x44e108e8
#define PIN_59			0x44e108ec
#define PIN_60			0x44e108f0
#define PIN_61			0x44e108f4
#define PIN_62			0x44e108f8
#define PIN_63			0x44e108fc
#define PIN_64			0x44e10900

#define PIN_65			0x44e10904
#define PIN_66			0x44e10908
#define PIN_67			0x44e1090c
#define PIN_68			0x44e10910
#define PIN_69			0x44e10914
#define PIN_70			0x44e10918
#define PIN_71			0x44e1091c
#define PIN_72			0x44e10920
#define PIN_73			0x44e10924
#define PIN_74			0x44e10928
#define PIN_75			0x44e1092c
#define PIN_76			0x44e10930
#define PIN_77			0x44e10934
#define PIN_78			0x44e10938
#define PIN_79			0x44e1093c
#define PIN_80			0x44e10940
#define PIN_81			0x44e10944
#define PIN_82			0x44e10948
#define PIN_83			0x44e1094c
#define PIN_84			0x44e10950
#define PIN_85			0x44e10954
#define PIN_86			0x44e10958
#define PIN_87			0x44e1095c
#define PIN_88			0x44e10960
#define PIN_89			0x44e10964
#define PIN_90			0x44e10968
#define PIN_91			0x44e1096c
#define PIN_92			0x44e10970
#define PIN_93			0x44e10974
#define PIN_94			0x44e10978 
#define PIN_95			0x44e1097c 
#define PIN_96			0x44e10980

#define PIN_97			0x44e10984
#define PIN_98			0x44e10988
#define PIN_99			0x44e1098c
#define PIN_100			0x44e10990
#define PIN_101			0x44e10994
#define PIN_102			0x44e10998
#define PIN_103			0x44e1099c
#define PIN_104			0x44e109a0
#define PIN_105			0x44e109a4
#define PIN_106			0x44e109a8
#define PIN_107			0x44e109ac
#define PIN_108			0x44e109b0
#define PIN_109			0x44e109b4
#define PIN_110			0x44e109b8
#define PIN_111			0x44e109bc
#define PIN_112			0x44e109c0
#define PIN_113			0x44e109c4
#define PIN_114			0x44e109c8
#define PIN_115			0x44e109cc
#define PIN_116			0x44e109d0
#define PIN_117			0x44e109d4
#define PIN_118			0x44e109d8
#define PIN_119			0x44e109dc
#define PIN_120			0x44e109e0
#define PIN_121			0x44e109e4
#define PIN_122			0x44e109e8
#define PIN_123			0x44e109ec
#define PIN_124			0x44e109f0
#define PIN_125			0x44e109f4
#define PIN_126			0x44e109f8
#define PIN_127			0x44e109fc

#define PIN_128			0x44e10a00
#define PIN_129			0x44e10a04
#define PIN_130			0x44e10a08
#define PIN_131			0x44e10a0c
#define PIN_132			0x44e10a10
#define PIN_133			0x44e10a14
#define PIN_134			0x44e10a18
#define PIN_135			0x44e10a1c
#define PIN_136			0x44e10a20
#define PIN_137			0x44e10a24
#define PIN_138			0x44e10a28
#define PIN_139			0x44e10a2c
#define PIN_140			0x44e10a30
#define PIN_141			0x44e10a34


#define PINMUX_PULLUDDISABLE		0x00000008 
#define PINMUX_PULLUPSEL		0x00000010
#define PINMUX_RXACTIVE			0x00000020 
#define PINMUX_SLOWSLEW			0x00000040
#define PINMUX_MUXMODE(n)		(n)

struct PINMUX_BITS {         		//bits		description
	uint32_t mmode:3;		//2:0		Pad mode
	uint32_t puden:1;		//3		Pad pullup/pulldown enable
	uint32_t putypesel:1;	 	//4		Pad pullup/pulldown type selection
	uint32_t rxactive:1;		//5		Input enable value for the PAD
	uint32_t slewctrl:1;		//6		Faster or slower slew rate
	uint32_t rsvd1:13;       	//19:7		reserved
	uint32_t rsvd2:12;       	//31:20		reserved
};

typedef union _PINMUX_REG {
   uint32_t				all;
   struct PINMUX_BITS	bit;
}PINMUX_REG;


#ifdef __cplusplus
}
#endif

#endif
