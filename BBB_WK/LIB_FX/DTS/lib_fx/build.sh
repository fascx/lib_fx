#!/bin/sh
mkdir out
dtc -O dtb -o LIBFX_ADC-00A0.dtbo -b 0 -@  LIBFX_ADC-00A0.dts
dtc -O dtb -o LIBFX_I2C1-00A0.dtbo -b 0 -@  LIBFX_I2C1-00A0.dts
dtc -O dtb -o LIBFX_I2C1A1-00A0.dtbo -b 0 -@  LIBFX_I2C1A1-00A0.dts
dtc -O dtb -o LIBFX_SPIDEV0-00A0.dtbo -b 0 -@  LIBFX_SPIDEV0-00A0.dts
dtc -O dtb -o LIBFX_UART1-00A0.dtbo -b 0 -@  LIBFX_UART1-00A0.dts
dtc -O dtb -o LIBFX_UART2-00A0.dtbo -b 0 -@  LIBFX_UART2-00A0.dts
dtc -O dtb -o LIBFX_UART4-00A0.dtbo -b 0 -@  LIBFX_UART4-00A0.dts
dtc -O dtb -o LIBFX_UART5-00A0.dtbo -b 0 -@  LIBFX_UART5-00A0.dts
dtc -O dtb -o LIBFX_EQEP0.dtbo -b 0 -@  LIBFX_EQEP0.dts
dtc -O dtb -o LIBFX_EQEP1.dtbo -b 0 -@  LIBFX_EQEP1.dts
dtc -O dtb -o LIBFX_EQEP2.dtbo -b 0 -@  LIBFX_EQEP2.dts
dtc -O dtb -o LIBFX_PWM_P8_13-00A0.dtbo -b 0 -@  LIBFX_PWM_P8_13-00A0.dts
dtc -O dtb -o LIBFX_PWM_P8_19-00A0.dtbo -b 0 -@  LIBFX_PWM_P8_19-00A0.dts
dtc -O dtb -o LIBFX_PWM_P8_34-00A0.dtbo -b 0 -@  LIBFX_PWM_P8_34-00A0.dts
dtc -O dtb -o LIBFX_PWM_P8_36-00A0.dtbo -b 0 -@  LIBFX_PWM_P8_36-00A0.dts
dtc -O dtb -o LIBFX_PWM_P8_45-00A0.dtbo -b 0 -@  LIBFX_PWM_P8_45-00A0.dts
dtc -O dtb -o LIBFX_PWM_P8_46-00A0.dtbo -b 0 -@  LIBFX_PWM_P8_46-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_14-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_14-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_16-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_16-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_21-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_21-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_22-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_22-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_28-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_28-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_29-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_29-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_31-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_31-00A0.dts
dtc -O dtb -o LIBFX_PWM_P9_42-00A0.dtbo -b 0 -@  LIBFX_PWM_P9_42-00A0.dts
mv *.dtbo out